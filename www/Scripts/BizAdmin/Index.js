﻿
$(document).ready(function () {

    // Event listener for opening and closing details
    $('tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblItem.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetItem(row.data().Item_ID, row);
            tr.addClass('shown');
        }
    });
});

function confirmDelete(t) {
    return window.confirm("Are you sure you would like to delete this? (" + $(t).data('item-id') + ")");
}

function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[1].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' };

    $.ajax({
        type: "GET",
        url: urlItemApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th:last').html('<button class="AddItem btn btn-sm btn-success float-right">Add</button>');

    $("button.AddItem").on("click", function (e) {
        window.location.href = urlItemCreate;
    });
}

function deleteItem(e) {
    var goDelete = confirmDelete(this); // window.confirm("Are you sure you would like to delete - " + $(this).data('item-id') + "?");

    if (!goDelete)
        return;
    $.ajax({
        url: urlItemApi.replace("-1", $(this).data('item-id')),
        type: 'DELETE',
        dataType: 'json',
        data: null
    }).done(function (data) {
        tblItems.api().draw();
    }).fail(function () {
        console.log('Error in Operation');
    });

}

function editItem(e) {
    var itemId = $(this).data('item-id');
    window.location.href = urlItemEdit.replace("-1", itemId);
}

function DrawCallback(settings) {    
    $("button.DeleteItem").on("click", deleteItem);

    $("button.EditItem").on("click", editItem );
}

function RenderActionLinks(val, type, row, meta) {
    var delString = String.format("<button data-item-id='{0}' class='DeleteItem btn btn-sm btn-danger'>Delete</button>", val);
    var editString = String.format("<button data-item-id='{0}' class='EditItem btn btn-sm btn-primary'>Edit</button>", val);
    return editString + '&nbsp;' + delString;
}

function RenderCheckbox(val, type, row) {
    if (type === 'display') {
        return (val ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>');
    }
    return val;
}

function RenderPlainText(val, type, row) {

    if (type === 'display') {
        try {
            return val.replace(/<[^>]*>?/gm, '');
        }
        catch {
            return val;
        }
    }
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function GetItem(ItemId, dtRow) {
    $.ajax({
        type: "GET",
        url: urlItemApi.replace('-1', ItemId),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}