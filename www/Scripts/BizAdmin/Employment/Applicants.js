﻿var tblEmployment;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}

$(document).ready(function () {

    var columnDefs = [
        { "data": "applicationdate", "title": "Application Date","searchable": false, "render": RenderDate },
        { "data": "applicantfirstname", "title": "First Name", "class": "no_wrap" },
        { "data": "applicantlastname", "title": "Last Name", "class": "no_wrap" },
        { "data": "applicantemail", "title": "Email", "class": "no_wrap" },
        { "data": "applicantposition", "title": "Position" },
        { "data": "finalphone", "title": "Phone" },
        { "data": "job_id", "title": "Job_ID", "visible": false },
        { "data": "applicationid", "title": "", "searchable": false, "orderable": false, "render": RenderViewAppLink, "visible": true },
        { "data": "applicationid", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteApplicationLink, "visible": true },
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblEmployment = $('#applicants').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        "headerCallback": HeaderCallback,
        "order": [0, 'desc'],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#applicants tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblEmployment.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetApplicantItem(row.data().applicationid, row);
            tr.addClass('shown');
        }
    });
});

function PopulateTable(data, callback, settings) {

    // add the job_id from the querystring    
    data.columns[6].search.value = job_id;

    $.ajax({
        type: "GET",
        url: urlApplicantsApi,
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th').eq(7).html('<button class="AddEmploymentItem btn btn-sm btn-success" >Add</button>');

    $("button.AddEmploymentItem").on("click", function (e) {
        window.location.href = urlEmployment_Create;
    });
}

function DrawCallback(settings) {
    $("button.DeleteApplicant").on("click", function (e) {

        var goDelete = window.confirm("Are you sure you would like to delete?");

        if (!goDelete)
            return;
        $.ajax({
            url: urlApplicantsApi +'/' + $(this).data('applicationid'),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblEmployment.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });


    $("button.ViewApplicants").on("click", function (e) {
        var applicationid = $(this).data('applicationid');
        window.location.href = urlApplicant_View.replace("-1", applicationid);
    });

}

function RenderViewAppLink(val, type, row) {
    var fmtString = "<button data-applicationid='{0}' class='ViewApplicants btn btn-sm btn-primary'>View</button>";
    return String.format(fmtString, row.applicationid);
}

function RenderDeleteApplicationLink(val, type, row) {
    var fmtString = "<button data-applicationid='{0}' class='DeleteApplicant btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.applicationid);
}


function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GetApplicantItem(applicationid, dtRow) {
    $.ajax({
        type: "GET",
        url: urlApplicantsApi.replace('-1', applicationid),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}