﻿var tblEmployment;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}

$(document).ready(function () {

    var columnDefs = [
        { "data": "date_Posted", "title": "Posted Date", "render": RenderDate },
        { "data": "Job_title", "title": "Title", "class": "no_wrap" },
        { "data": "When_available", "title": "Availability", "class": "no_wrap" },
        { "data": "Qualifications", "title": "Qualification", "class": "no_wrap" },
        { "data": "Contact", "title": "Contact Person" },
        { "data": "Part_Time", "title": "Part Time" },
        { "data": "Active", "title": "Active", "class": "no_wrap" },
      //  { "data": null, "defaultContent": "", "searchable": false, "orderable": false, "class": "details-control" },
        { "data": "Job_ID", "title": "", "searchable": false, "orderable": false, "render": RenderViewAppLink, "visible": true },
        { "data": "Job_ID", "title": "", "searchable": false, "orderable": false, "render": RenderEditEmpItemLink, "visible": true },
        { "data": "Job_ID", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteEmpItemLink, "visible": true }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblEmployment = $('#employment').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        "headerCallback": HeaderCallback,
        "order": [0, 'desc'],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#employment tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblEmployment.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetEmpItem(row.data().Job_ID, row);
            tr.addClass('shown');
        }
    });
});

function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[1].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' }

    $.ajax({
        type: "GET",
        url: urlEmploymentsApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th').eq(7).html('<button class="AddEmploymentItem btn btn-sm btn-success" >Add</button>');

    $("button.AddEmploymentItem").on("click", function (e) {
        window.location.href = urlEmployment_Create;
    });
}

function DrawCallback(settings) {
    $("button.DeleteEmploymentItem").on("click", function (e) {

        var goDelete = window.confirm("Are you sure you would like to delete?");

        if (!goDelete)
            return;
        alert(urlEmploymentsApi + " " + $(this).data('employmentId'));
        $.ajax({
            url: urlEmploymentsApi.replace("-1", $(this).data('employmentId')),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblEmployment.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });

    $("button.EditEmploymentItem").on("click", function (e) {
        var employmentId = $(this).data('employmentId');
        window.location.href = urlEmployment_Edit.replace("-1", employmentId);
    });

    $("button.ViewApplicants").on("click", function (e) {
        var employmentId = $(this).data('employmentId');
        window.location.href = urlApplicantView.replace("-1", employmentId);
    });

}

function RenderViewAppLink(val, type, row) {
    var fmtString = "<button data-employment-id='{0}' class='ViewApplicants btn btn-sm btn-primary'>Applicants</button>";
    return String.format(fmtString, row.Job_ID);
}

function RenderDeleteEmpItemLink(val, type, row) {
    var fmtString = "<button data-employment-id='{0}' class='DeleteEmploymentItem btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.Job_ID);
}

function RenderEditEmpItemLink(val, type, row) {
    var fmtString = "<button data-employment-id='{0}' class='EditEmploymentItem btn btn-sm btn-primary'>Edit</button>";
    return String.format(fmtString, row.Job_ID);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GetEmpItem(employmentId, dtRow) {
    $.ajax({
        type: "GET",
        url: urlEmploymentsApi.replace('-1', employmentId),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}