﻿var tblContact;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}


$(document).ready(function () {

    var columnDefs = [
        { "data": "ContactDateTime", "title": "Date/Time", "render": RenderDate },
        { "data": "FirstName", "title": "First Name", "class": "no_wrap" },
        { "data": "LastName", "title": "Last Name", "class": "no_wrap" },
        { "data": "Email", "title": "Email", "class": "no_wrap" },
        { "data": "Phone", "title": "Phone" },
        { "data": "Department", "title": "Department" },
        { "data": "Comments", "title": "Comments" },
        //{ "data": "SearchEngine", "title": "Search Engine", "class": "no_wrap" },
        //{ "data": "SearchPhrase", "title": "Search Phrase", "class": "no_wrap" },
        { "data": null, "defaultContent": "", "searchable": false, "orderable": false, "class": "details-control", "visible": false },
        { "data": "ID", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteEventItemLink, "visible": true }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblContact = $('#contacts').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        //"headerCallback": HeaderCallback,
        "aaSorting": [[1, 'desc']],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#contacts tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblContact.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GeContactItem(row.data().ID, row);
            tr.addClass('shown');
        }
    });
});


function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[2].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' }
    $.ajax({
        type: "GET",
        url: urlContactApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}


function DrawCallback(settings) {
    $("button.DeleteContactItem").on("click", function (e) {
        var goDelete = window.confirm("Are you sure you would like to delete this contact?");

        if (!goDelete)
            return;
        $.ajax({
            url: urlContactApi.replace("-1", $(this).data('contactId')),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblContact.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });
}

function RenderDeleteEventItemLink(val, type, row) {
    var fmtString = "<button data-contact-id='{0}' class='DeleteContactItem btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.ContactId);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GeContactItem(ContactItemId, dtRow) {
    $.ajax({
        type: "GET",
        url: urlContactApi.replace('-1', ContactItemId),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}