﻿var tblUser;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}

$(document).ready(function () {
    var columnDefs = [
        { "data": "UserName", "title": "User Name", "class": "no_wrap" },
        { "data": "FirstName", "title": "First Name", "class": "no_wrap" },
        { "data": "LastName", "title": "Last Name", "class": "no_wrap" },
        { "data": "Email", "title": "Email", "class": "no_wrap" },
         { "data": null, "defaultContent": "", "searchable": false, "orderable": false, "class": "details-control","visible":false },
        { "data": "UserName", "title": "", "searchable": false, "orderable": false, "render": RenderEditUsersItemLink, "visible": true },
        { "data": "UserName", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteUsersItemLink, "visible": true }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblUser = $('#UserManager').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        "headerCallback": HeaderCallback,
        "order": [0, 'desc'],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#UserManager tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblUser.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetUsersItem(row.data().UserName, row);
            tr.addClass('shown');
        }
    });
});

function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    //data.columns[0].search = data.search;
    //Remove the original search. Just because.
    //data.search = { value: '', regex: '' }

    $.ajax({
        type: "GET",
        url: urlUsersApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th').eq(5).html('<button class="AddUserItem btn btn-sm btn-success" >Add</button>');

    $("button.AddUserItem").on("click", function (e) {
        window.location.href = urlUsers_Create;
    });
}

function DrawCallback(settings) {
    $("button.DeleteUserItem").on("click", function (e) {

        var goDelete = window.confirm("Are you sure you would like to delete the user details?");

        if (!goDelete)
            return;
        $.ajax({
            // username is base64 encoded to avoid issues with special characters, e.g. the @ symbol
            url: urlUsersApi.replace("-1", btoa($(this).data('userName'))),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblUser.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });

    $("button.EditUserItem").on("click", function (e) {
        var userName = $(this).data('userName');
        window.location.href = urlUsers_Edit.replace("-1", userName);
    });
}

function RenderDeleteUsersItemLink(val, type, row) {
    var fmtString = "<button data-user-name='{0}' class='DeleteUserItem btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.UserName);
}

function RenderEditUsersItemLink(val, type, row) {
    var fmtString = "<button data-user-name='{0}' class='EditUserItem btn btn-sm btn-primary'>Edit</button>";
    return String.format(fmtString, row.UserName);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GetUsersItem(userName, dtRow) {
    $.ajax({
        type: "GET",
        url: urlUsers_Create.replace('-1', userName),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}