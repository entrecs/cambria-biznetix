﻿var tblLocation;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}


$(document).ready(function () {

    var columnDefs = [
        { "data": "Name", "title": "Company Name", "class": "no_wrap" },
        { "data": "Address", "title": "Address", "class": "no_wrap" },
        { "data": "City", "title": "City" },
        { "data": "State", "title": "State" },
        { "data": "Zip", "title": "ZIP", "class": "no_wrap" },
        { "data": "Latitude", "title": "Latitude", "class": "no_wrap" },
        { "data": "Longitude", "title": "Longitude", "class": "no_wrap" },
        { "data": "Active", "title": "Active", "class": "no_wrap" },
         { "data": "LocationID", "title": "", "searchable": false, "orderable": false, "render": RenderEditEventItemLink, "visible": true },
        { "data": "LocationID", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteEventItemLink, "visible": true }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblLocation = $('#locations').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        //"headerCallback": HeaderCallback,
        "aaSorting": [[0, 'desc']],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#locations tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblLocation.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetLocation(row.data().LocationID, row);
            tr.addClass('shown');
        }
    });
});


function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[0].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' }
    $.ajax({
        type: "GET",
        url: urlLocationApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th').eq(8).html('<button class="AddEventItem">Add</button>');

    $("button.AddEventItem").on("click", function (e) {
        window.location.href = urlEventitem_Create;
    });
}
function DrawCallback(settings) {
    $("button.DeleteLocation").on("click", function (e) {
        var goDelete = window.confirm("Are you sure you would like to delete?");

        if (!goDelete)
            return;
        $.ajax({
            url: urlLocationApi.replace("-1", $(this).data('eventId')),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblLocation.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });

    $("button.EditLocation").on("click", function (e) {
        //alert(urlEventsApi + " " + $(this).data('eventId'));
        var eventId = $(this).data('eventId');
        window.location.href = urlEventitem_Edit.replace("-1", eventId);
    });
}

function RenderDeleteEventItemLink(val, type, row) {
    var fmtString = "<button data-event-id='{0}' class='DeleteLocation btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.LocationID);
}
function RenderEditEventItemLink(val, type, row) {
    var fmtString = "<button data-event-id='{0}' class='EditLocation btn btn-sm btn-success'>Edit</button>";
    return String.format(fmtString, row.LocationID);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GetLocation(LocationId, dtRow) {
    $.ajax({
        type: "GET",
        url: urlLocationApi.replace('-1', LocationId),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}