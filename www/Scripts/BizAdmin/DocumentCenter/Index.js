﻿var tblDocument;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}


$(document).ready(function () {
    var columnDefs = [
        { "data": "Title", "title": "Title", "render": RenderDocFileItemLink },
         { "data": "FileSize", "title": "File Size", "class": "no_wrap" },
        { "data": "DateTime", "title": "Date Added", "render": RenderDate }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblDocument = $('#document').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        //"drawCallback": DrawCallback,
        //"headerCallback": HeaderCallback,
        "order": [0, 'desc'],
        "processing": true,

        "serverSide": true,
    });
    $('#chk_showInactive').change(function () {
        tblDocument.api().draw();
    });
    // Event listener for opening and closing details
    $('#document tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblDocument.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            //GetDocsItem(row.data().FileID, row);
            tr.addClass('shown');
        }
    });
});

function PopulateTable(data, callback, settings) {
    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[0].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' }
    //data = { dtRequest: data, active: isactive };
    $.ajax({
        type: "GET",
        url: urlDocumentsApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

//function HeaderCallback(thead, data, start, end, display) {
//    $(thead).find('th').eq(5).html('<button class="AddDocumentItem btn btn-sm btn-success" >Add</button>');
//    $("button.AddDocumentItem").on("click", function (e) {
//        window.location.href = urlDocuments_Create;
//    });
//}

//function DrawCallback(settings) {
//    $("button.DeleteDocumentItem").on("click", function (e) {

//        var goDelete = window.confirm("Are you sure you want to delete?");

//        if (!goDelete)
//            return;
//        alert(urlDocumentsApi + " " + $(this).data('fileId'));
//        $.ajax({
//            url: urlDocumentsApi.replace("-1", $(this).data('fileId')),
//            type: 'DELETE',
//            dataType: 'json',
//            data: null
//        }).done(function (data) {
//            tblDocument.api().draw();
//        }).fail(function () {
//            console.log('Error in Operation');
//        });
//    });

//    $("button.EditDocumentItem").on("click", function (e) {
//        var fileId = $(this).data('fileId');
//        window.location.href = urlDocuments_Edit.replace("-1", fileId);
//    });
//    $("a.PreviewDocItem").on("click", function (e) {
//        var folderId = $(this).data('fileId');
//        ReadFile($(this).data('fileId'));
//    });
//    $("a.ViewLinkUrl").on("click", function (e) {
//        var fileNameExt = $(this).data('fileName').split('.')[1];
//        window.open(this.text + "." + fileNameExt, "_blank", "", "");
//    });

//}
//function ReadFile(id) {
//    $.ajax({
//        contentType: 'application/json;',
//        dataType: 'json',
//        type: 'POST',
//        url: '/Bizadmin/FileManager/ReadFile?Id=' + id,
//        success: function (data) {
//            //var filepath = String.format("{0}{1}_{2}/{3}", '/Content/Documents/',data.folderName,data.FolderID,data.file.FileName)            
//            var filename = data.file.FileName;

//            $('#lblFilename').text(filename);
//            $("#lblfileId").text(data.file.FileID);
//        },
//        failure: function (response) {
//            alert("save failed");
//        }
//    });
//}
function RenderDeleteDocsItemLink(val, type, row) {
    var fmtString = "<button data-file-id='{0}' class='DeleteDocumentItem btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, val);
}
function RenderDocFileItemLink(val, type, row) {
    var publiclinkurl = '';
    if (row.LinkUrl != null || row.LinkUrl != null) {
        publiclinkurl = "http://" + window.location.host + "/Bizadmin/FileManager?FileName=" + row.LinkUrl;
    }
    var fmtString = "<a data-file-id='{0}' onclick='openFile(\"" + publiclinkurl + "\",\"" + row.LinkUrl + "\")'  target='_blank' data-file-link='{1}'   class='PreviewDocItem no_wrap col-md-3' >" + val + "</a>";
    return String.format(fmtString, val, row.LinkUrl);
}
function openFile(fileurl, fileName) {
    if (fileurl != "") {
        CheckFileExistorNot(fileName);
    }
}
function CheckFileExistorNot(fileName) {
    $.ajax({
        contentType: 'application/json;',
        dataType: 'json',
        type: 'POST',
        url: '/Bizadmin/FileManager/IsFileExist?fileName=' + fileName,
        success: function (data) {
            if (!data.IsExist) {
                alert("file is not found.");
            }
            else {
                window.open("http://" + window.location.host + "/Bizadmin/FileManager?FileName=" + fileName, "_blank");
            }
            return data.IsExist;
        },
        failure: function (response) {
            alert("save failed");
        }
    });    
}
function RenderDocFileLinkUrl(val, type, row) {
    var fmtString = '';
    if (val != '' && val != null) {
        fmtString = "<a data-file-name='{0}' Title='" + window.location.host + "/ContentPages/Documents/SharedDocs/" + val + "'class='ViewLinkUrl no_wrap' data-toggle='modal' >" + window.location.host + "/ContentPages/Documents/SharedDocs/" + val + "</a>";
    }
    return String.format(fmtString, row.FileName);
}
function RenderEditDocsItemLink(val, type, row) {
    var fmtString = "<button data-file-id='{0}' class='EditDocumentItem btn btn-sm btn-primary'>Edit</button>";
    return String.format(fmtString, val);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

//function GetDocsItem(fileId, dtRow) {
//    $.ajax({
//        type: "GET",
//        url: urlDocuments_Create.replace('-1', fileId),
//        data: null
//    }).done(function (data) {
//        dtRow.child($("<div />").html(data.Body)).show();
//    });
//}