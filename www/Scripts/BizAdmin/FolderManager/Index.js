﻿var tblDocument;

//TODO: Toss this in a global javascript file
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match
            ;
        });
    };
}

$(document).ready(function () {
    var columnDefs = [
        { "data": "FolderName", "title": "Folder Name", "render": RenderDocsItemLink },
        { "data": "FolderDesc", "title": "Description", "class": "no_wrap" },
        { "data": "DateAdded", "title": "Date Added", "render": RenderDate },
        { "data": "FolderID", "title": "", "searchable": false, "orderable": false, "render": RenderEditDocsItemLink, "visible": true },
        { "data": "FolderID", "title": "", "searchable": false, "orderable": false, "render": RenderDeleteDocsItemLink, "visible": true }
    ];

    //Autogenerate targets and name members for the columnDefs object
    for (index = 0; index < columnDefs.length; index++) {
        columnDefs[index].targets = index;
        columnDefs[index].name = columnDefs[index].data;
    }

    tblDocument = $('#document').dataTable({
        "ajax": PopulateTable,
        "columnDefs": columnDefs,
        "drawCallback": DrawCallback,
        "headerCallback": HeaderCallback,
        "order": [0, 'desc'],
        "processing": true,
        "serverSide": true
    });

    // Event listener for opening and closing details
    $('#document tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tblDocument.api().row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            GetDocsItem(row.data().FolderID, row);
            tr.addClass('shown');
        }
    });
});

function PopulateTable(data, callback, settings) {

    //HACK: Modify the global search to only search the title column.
    //This is due to the fact that the backend currently doesn't recognize the 
    //global search properties in the object that gets sent back.
    data.columns[0].search = data.search;
    //Remove the original search. Just because.
    data.search = { value: '', regex: '' }

    $.ajax({
        type: "GET",
        url: urlDocumentsApi.replace('-1', ''),
        data: data
    }).done(function (data) {
        callback(data);
    });
}

function HeaderCallback(thead, data, start, end, display) {
    $(thead).find('th').eq(3).html('<button class="AddDocumentItem btn btn-sm btn-success" >Add Folder</button>');

    $("button.AddDocumentItem").on("click", function (e) {
        window.location.href = urlDocuments_Create;
    });
}

function DrawCallback(settings) {
    $("button.DeleteDocumentItem").on("click", function (e) {

        var goDelete = window.confirm("Are you sure you want to delete this folder and contents?");

        if (!goDelete)
            return;
        else
            var confirm = window.confirm("If you delete this, all files in the folder will be deleted!");

        if (!confirm)
            return;
       // alert(urlDocumentsApi + " " + $(this).data('folderId'));
        $.ajax({
            url: urlDocumentsApi.replace("-1", $(this).data('folderId')),
            type: 'DELETE',
            dataType: 'json',
            data: null
        }).done(function (data) {
            tblDocument.api().draw();
        }).fail(function () {
            console.log('Error in Operation');
        });
    });

    $("button.EditDocumentItem").on("click", function (e) {
        var folderId = $(this).data('folderId');
        window.location.href = urlDocuments_Edit.replace("-1", folderId);
    });
    $("a.ViewDocumentFolderItem").on("click", function (e) {
        var folderId = $(this).data('folderId');
        GetFiles_Folder($(this).data('folderId'));
    });
}
function GetFiles_Folder(id) {
    $.ajax({
        contentType: 'application/json;',
        dataType: 'json',
        type: 'POST',
        url: '/Bizadmin/FolderManager/GetFiles?Id=' + id,
        success: function (data) {
            var folder = data.folder;
            var files = data.file;
            $("div.modal-body").html('');
            for (var i = 0; i <= files.length; i++) {
                var fileExtension = files[i].FileName.split('.')[1];
                if (fileExtension == "png" || fileExtension == "jpg" || fileExtension == "gif") {
                    $("div.modal-body").append("<div id='" + files[i].FileID + "' class='img-fluid col-md-3 left'><img src='/ContentPages/Documents/" + folder.FolderName + "_" + folder.FolderID + "/" + files[i].FileName.split('.')[0] + "." + files[i].FileName.split('.')[1] + "' class='img-fluid thumbnail' id='" + files[i].FileID + "' alt='Uploaded Image' /><label id='" + "iblid_" + data.file.FileID + "' value='" + files[i].FileName + "'>" + files[i].FileName + "</label></div>");
                }
                else {
                    $("div.modal-body").append("<div id='" + files[i].FileID + "' title='" + files[i].FileName + "' class='img-fluid col-md-3'><img src='/images/icon_file.png' class='img-fluid thumbnail' id='" + files[i].FileID + "' alt='Uploaded Image' /><label id='" + "iblid_" + data.file.FileID + "' value='" + files[i].FileName + "'>" + files[i].FileName + "</label></div>");
                }
            };
        },
        failure: function (response) {
            alert("save failed");
        }
    });
}
function RenderDeleteDocsItemLink(val, type, row) {
    var fmtString = "<button data-folder-id='{0}' class='DeleteDocumentItem btn btn-sm btn-danger'>Delete</button>";
    return String.format(fmtString, row.FolderID);
}
function RenderDocsItemLink(val, type, row) {
    var fmtString = "<a data-folder-id='{0}' class='ViewDocumentFolderItem no_wrap' data-toggle='modal' data-target='#FolderView' >" + val + "</a>";
    return String.format(fmtString, row.FolderID);
}

function RenderEditDocsItemLink(val, type, row) {
    var fmtString = "<button data-folder-id='{0}' class='EditDocumentItem btn btn-sm btn-primary'>Edit</button>";
    return String.format(fmtString, row.FolderID);
}

function RenderDate(val, type, row) {
    if (type === 'display') {
        return (val == null ? '' : moment(val).format('L'));
    } else if (type === 'sort') {
        return (val == null ? '' : moment(val).unix());
    }

    return val;
}

function GetDocsItem(folderId, dtRow) {
    $.ajax({
        type: "GET",
        url: urlDocuments_Create.replace('-1', folderId),
        data: null
    }).done(function (data) {
        dtRow.child($("<div />").html(data.Body)).show();
    });
}