﻿var UNITED_WAY_ONTARIO_PAYMENT_PREFIX = 'UWOC-';

$(function () {
    var form = document.getElementById('donation_form')
    var order;

    paypal.Buttons({
        // onInit is called when the button first renders
        onInit: function (data, actions) {

            // Disable the buttons
            actions.disable();

            $('.required-input').change(function (e) {
                var inputElement = e.target;

                if (inputElement.checkValidity() === true) {
                    inputElement.classList.remove('invalid-input');
                }

                if ($('.invalid-input').length === 0) {
                    actions.enable();
                } else {
                    actions.disable();
                }
            });
        },

        // onClick is called when the button is clicked
        onClick: function () {

            // Show a validation error if the checkbox is not checked
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        },
        createOrder: function (data, actions) {
            order = {
                intent: 'CAPTURE',
                payer: {
                    address: {
                        address_line_1: $('#street_address_1').val(),
                        address_line_2: $('#street_address_2').val(),
                        admin_area_2: $('#city').val(), // city
                        admin_area_1: $('#state').val(), // state
                        postal_code: $('#zip').val(),
                        country_code: 'US'
                    },
                    email_address: $('#email').val(),
                    name: {
                        given_name: $('#first_name').val(),
                        surname: $('#last_name').val()
                    },
                    phone: {
                        phone_type: 'MOBILE',
                        phone_number: {
                            national_number: $('#phone').val(),
                        }
                    }
                },
                shipping_info: {
                    phone: {
                        phone_type: 'MOBILE',
                        phone_number: {
                            national_number: $('#phone').val(),
                        }
                    }
                },
                purchase_units: [{
                    amount: {
                        currency_code: 'USD',
                        value: $('#amount').val()
                    },
                    description: UNITED_WAY_ONTARIO_PAYMENT_PREFIX + $('#donor_directive').val(),
                    soft_descriptor: 'OntCnty'
                }],
                    application_context: {
                landing_page: 'BILLING',
                    user_action: 'PAY_NOW'
            }
        };
      
            console.log(JSON.stringify(order));
            return actions.order.create(order);

        },
        onApprove: function (data, actions) {
            console.log("In onApprove.");
            return actions.order.capture().then(function (details) {
                console.log("DETAILS: ", details);
                console.log("ORDER: ", order);

                postDonationToAppServer(order, details);
            });
        }
    }).render('#paypal-button-container');

    function postDonationToAppServer(userInput, payPalDetails) {
        var contactInfo = {
            FirstName: userInput.payer.name.given_name,
            LastName: userInput.payer.name.surname,
            Email: userInput.payer.email_address,
            Phone: userInput.payer.phone.phone_number.national_number,
            Address: userInput.payer.address.address_line_1,
            City: userInput.payer.address.admin_area_2,
            State: userInput.payer.address.admin_area_1,
            ZIP: userInput.payer.address.postal_code,
            ContactDateTime: moment(payPalDetails.create_time).format('YYYY-MM-DD HH:mm:ss')
        };

        var addressTwo = userInput.payer.address.address_line_2;
        var comments = $('#comments').val();
        var company = $('#employer').val();

        if (addressTwo.length) {
            contactInfo.Address += '; ' + addressTwo; 
        }

        if (comments) {
            contactInfo['Comments'] = comments;
        }

        if (company) {
            contactInfo['Company'] = company;
        }

        var donationDetails = {
            Amount: parseFloat(parseFloat(payPalDetails.purchase_units[0].amount.value).toFixed(2)),
            OrderCreated: moment(payPalDetails.create_time).format('YYYY-MM-DD HH:mm:ss'),
            OrderId: payPalDetails.id,
            ReferenceUrl: payPalDetails.links[0].href,
            PaymentId: payPalDetails.purchase_units[0].payments.captures[0].id,
            PayerId: payPalDetails.payer.payer_id,
            Status: payPalDetails.status
        };

        var merchant = payPalDetails.purchase_units[0].payee;
        var directive = $('#donor_directive').val();

        if (merchant.email_address) {
            donationDetails['MerchantEmail'] = merchant.email_adress;
        }

        if (merchant.merchant_id) {
            donationDetails['MerchantId'] = merchant.merchant_id;
        }

        if (directive) {
            donationDetails['Directive'] = directive;
        }

        var donationReceipt = {
            ContactInfo: contactInfo,
            DonationDetails: donationDetails
        };

        $.ajax({
            url: '/Donate/DonationCompleted',
            data: JSON.stringify(donationReceipt),
            type: 'POST',
            contentType: 'application/json',
            success: function (res) {
                console.log(res)
            },
            error: function (res, status, error) {
                console.log('ERROR ', res, status, error);
            }
        })
            .fail(function (res1, res2, res3) {
                console.log('REQ FAILED ', res1, res2, res3);
            });
    }


    // validations + data formatting / synchronization
    var $amountInput = $('#amount');
    
    $('#other-donate').click(function () {
        $amountInput.focus();
    });

    var prevAmountVal;
    $amountInput.focus(function () {
        prevAmountVal = $amountInput.val();
        $amountInput.val('');

        var $donateButtons = $('.changeDonationAmount');

        $donateButtons.removeClass('btn-primary');
        $('#other-donate').addClass('btn-primary');

        $amountInput.blur(function () {
            if (document.getElementById('amount').checkValidity() === false) {
                $amountInput.val(prevAmountVal);
            }

            var newDonateAmount = $amountInput.val();

            switch (newDonateAmount) {
                case '25.00':
                    $donateButtons.removeClass('btn-primary');
                    $($donateButtons[0]).addClass('btn-primary')
                    break;
                case '50.00':
                    $donateButtons.removeClass('btn-primary');
                    $($donateButtons[0]).addClass('btn-primary')
                    break;
                case '100.00':
                    $donateButtons.removeClass('btn-primary');
                    $($donateButtons[0]).addClass('btn-primary')
                    break;
                default:
                    break;
            }

            $("#total_amount_label").text(newDonateAmount);
        });
    });

    $amountInput.change(function () {
        $(this).val(parseFloat($(this).val()).toFixed(2));
    });


    var $phoneInput = $('#phone');
    var phoneDOM = document.getElementById('phone');
    $phoneInput.change(function () {
        var formattedPhone = $phoneInput.val().match(/[0-9]/g).join('');
        if (/^[0][1-9]\d{9}$|^[1-9]\d{9}$/g.test(formattedPhone)) {
            $phoneInput.val(formattedPhone);
            phoneDOM.setCustomValidity('');
        } else {
            phoneDOM.setCustomValidity('Please enter valid 10 digit phone number.');
        }
    });

    $('.changeDonationAmount').click(function () {
        $amountInput.val($(this).attr('data-suggested-amount'));
        $("#total_amount_label").text($(this).attr('data-suggested-amount'));
        $(this).addClass('btn-primary');
        $(this).siblings().removeClass('btn-primary');
        return false;
    });

    $('.changeFrequency').click(function () {
        $('#recurring_frequency').val($(this).attr('data-freq'));
        if ($('#recurring_frequency').val() == 'one_time') {
            $('#recurring_terms').collapse('hide');
        }
        else {
            $('#recurring_terms').collapse('show');
        }
        $(this).addClass('btn-primary');
        $(this).siblings().removeClass('btn-primary');
        return false;
    });
})