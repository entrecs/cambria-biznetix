﻿IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_SCHEMA = N'dbo' AND TABLE_NAME = N'FormSubmissions')
BEGIN
	SET ANSI_NULLS ON

	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[FormSubmissions](
		[FormSubmissionID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](255) NULL,
		[Address] [nvarchar](max) NULL,
		[City] [nvarchar](255) NULL,
		[State] [nvarchar](255) NULL,
		[Zip] [nvarchar](50) NULL,
		[Phone] [nvarchar](50) NULL,
		[Email] [nvarchar](255) NULL,
		[MiscJson] [nvarchar](max) NULL,
	 CONSTRAINT [PK_FormSubmission] PRIMARY KEY CLUSTERED 
	(
		[FormSubmissionID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

