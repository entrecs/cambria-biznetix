﻿BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings]
                   WHERE [Key] = 'EnableEventSubmissions'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('EnableEventSubmissions', 'True', 'TRUE-FALSE')
   END
END