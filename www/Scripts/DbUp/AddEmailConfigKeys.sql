﻿BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'FormEmailFrom'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('FormEmailFrom', 'user-form@townofcambria.com', 'TEXT')
   END
END

BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'SMTPDefaultSender'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('SMTPDefaultSender', 'townclerk@townofcambria.com', 'TEXT')
   END
END

BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'ContactEmailFrom'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('ContactEmailFrom', 'contact-page@townofcambria.com', 'TEXT')
   END
END

BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'ContactEmailTo'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('ContactEmailTo', 'lschlemmer@townofcambria.com', 'TEXT')
   END
END


GO
