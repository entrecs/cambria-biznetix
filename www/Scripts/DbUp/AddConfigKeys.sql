﻿USE [BizCMS_Cambria]
GO

BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'UseCaptcha'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('UseCaptcha', 'True', 'TRUE-FALSE')
   END
END

BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[configuration_settings] 
                   WHERE [Key] = 'FormEmailTo'
				   )
   BEGIN
       INSERT INTO [dbo].[configuration_settings] ([Key], [Value], [Type])
       VALUES ('FormEmailTo', 'lschlemmer@townofcambria.com', 'TEXT')
   END
END

GO


