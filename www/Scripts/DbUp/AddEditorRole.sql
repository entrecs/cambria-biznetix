﻿BEGIN
   IF NOT EXISTS (SELECT * FROM [dbo].[AspNetRoles] WHERE [Id] = 3)
   BEGIN
       INSERT INTO [dbo].[AspNetRoles] ([Id], [Name])
       VALUES (3, 'TrustedEditor')
   END
END