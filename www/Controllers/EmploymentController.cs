﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Helpers;
using Biznetix.Common.StringExtensions;

namespace Biznetix.BizCMS.Controllers
{
    public class EmploymentController : Controller
    {
        // GET: Employment
        [LoadActionFilters]
        public ActionResult Index()
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Employment listings are disabled.");
            }
            
            try
            {
                ViewBag.Employment_Content = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "Employment.inc");
            }
            catch { }
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                ViewBag.Editable = true;
            }
            else
            {
                ViewBag.Editable = false;
            }
            List<Job_Listing> jobList = Job_Listing.Fetch("").Where(x => x.Active == true).ToList();
            return View(jobList);
        }


        [LoadActionFilters]
        public ActionResult EmploymentDetail(int Id)
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Employment listings are disabled.");
            }
            
            Job_Listing jobList_item = Job_Listing.Fetch("").Where(x => x.Active == true && x.Job_ID == Id).FirstOrDefault();
            if (jobList_item == null)
                return RedirectToAction("Index","Employment",null);
            return View(jobList_item);
        }

        [LoadActionFilters]
        public ActionResult EmploymentApplicationForm(int? jobid)
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Employment listings are disabled.");
            }
            
            ViewBag.Locations = Job_Location.Fetch("");

            ViewBag.OpenJobs = Job_Listing.Fetch("WHERE active=1 order by date_Posted");

            List<configuration_setting> settings = configuration_setting.Fetch("");
            ViewBag.ConfigSetting = configuration_setting.Fetch("");

            ViewBag.EmploymentAppBackgroundInfo = GlobalSettings.config_settings["EmploymentAppBackgroundInfo"].ToBoolean();
            ViewBag.EmploymentAppCollectSSN = GlobalSettings.config_settings["EmploymentAppCollectSSN"].ToBoolean();
            ViewBag.EmploymentAppEducationHistory = GlobalSettings.config_settings["EmploymentAppEducationHistory"].ToBoolean();
            ViewBag.EmploymentAppEmploymentHistory = GlobalSettings.config_settings["EmploymentAppEmploymentHistory"].ToBoolean();
            ViewBag.EmploymentAppJobData = GlobalSettings.config_settings["EmploymentAppJobData"].ToBoolean();
            ViewBag.EmploymentAppPersonalData = GlobalSettings.config_settings["EmploymentAppPersonalData"].ToBoolean();
            ViewBag.EmploymentAppReferences = GlobalSettings.config_settings["EmploymentAppReferences"].ToBoolean();
            Job_Application newapp = new Job_Application();
            newapp.applicationdate = DateTime.Today;
            newapp.job_id = jobid;
            return View(newapp);
        }
        [HttpPost]
        public ActionResult EmploymentApplicationForm(Job_Application applicantDetails)
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Employment listings are disabled.");
            }
            
            if (applicantDetails != null)
            {

                applicantDetails.applicationdate = DateTime.Now;
                applicantDetails.Insert();

                List<configuration_setting> settings = configuration_setting.Fetch("");
                ViewBag.ConfigSetting = configuration_setting.Fetch("");

                ViewBag.EmploymentAppBackgroundInfo = GlobalSettings.config_settings["EmploymentAppBackgroundInfo"].ToBoolean();
                ViewBag.EmploymentAppCollectSSN = GlobalSettings.config_settings["EmploymentAppCollectSSN"].ToBoolean();
                ViewBag.EmploymentAppEducationHistory = GlobalSettings.config_settings["EmploymentAppEducationHistory"].ToBoolean();
                ViewBag.EmploymentAppEmploymentHistory = GlobalSettings.config_settings["EmploymentAppEmploymentHistory"].ToBoolean();
                ViewBag.EmploymentAppJobData = GlobalSettings.config_settings["EmploymentAppJobData"].ToBoolean();
                ViewBag.EmploymentAppPersonalData = GlobalSettings.config_settings["EmploymentAppPersonalData"].ToBoolean();
                ViewBag.EmploymentAppReferences = GlobalSettings.config_settings["EmploymentAppReferences"].ToBoolean();

                ViewBag.OpenJobs = Job_Listing.Fetch("");
                ViewBag.Locations = Job_Location.Fetch("");
                ViewBag.layout4email = true;
                String mail_body = RenderViewToString(this.ControllerContext, "~/Views/Employment/EmploymentApplicationForm.cshtml", applicantDetails, true);
                SendEmail.Send(GlobalSettings.config_settings["EmploymentApplicationsEmailTo"], mail_body, GlobalSettings.config_settings["EmploymentApplicationsEmailSubject"], GlobalSettings.config_settings["EmploymentApplicationsEmailFrom"], "");

            }
            return View("Application-Thank-You");
        }
        public ActionResult uploadPartial()
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Employment listings are disabled.");
            }
            var appData = Server.MapPath("~/ContentPages/Editoruploads");
            var images = Directory.GetFiles(appData).Select(x => new Biznetix.BizCMS.Helpers.ImagesViewModel
            {
                Url = Url.Content("/ContentPages/Editoruploads/" + Path.GetFileName(x))
            });
            return View(images);
        }

        static string RenderViewToString(ControllerContext context,
                            string viewPath,
                            object model = null,
                            bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

   

    }
}