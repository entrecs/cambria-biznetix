﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using Biznetix.BizCMS.Models;
using Recaptcha.Web;
using Recaptcha.Web.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Security;
using Biznetix.BizCMS;
using Biznetix.Common.StringExtensions;

namespace BiznetixCMS.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index(string id)
        {
            string cleanUrl = id;
            string pageName;

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];

            Biznetix.BizCMS.Data.AspNetUser user = AspNetUser.Fetch("Where UserName=@0", User.Identity.Name).FirstOrDefault();

            if (String.IsNullOrEmpty(cleanUrl)) cleanUrl = "default";
            if (cleanUrl.IndexOf("?") >= 0) cleanUrl.Remove(cleanUrl.IndexOf("?"));

            ViewBag.Editable = false;

            cms_Page thispage = allpages.FirstOrDefault(x => String.Equals(x.CleanUrl,cleanUrl,StringComparison.OrdinalIgnoreCase));
            if (thispage != null)
            {
                ViewBag.PageID = thispage.PageID;
                ViewBag.ParentPage = thispage.ParentPage;
                ViewBag.Mainpage = thispage.PageName;
                ViewBag.Filename = thispage.Filename;
                ViewBag.pageName = thispage.PageName;
                ViewBag.Header = thispage.pHeader ; 
                ViewBag.Title = thispage.Title;

                //meta tags 
                ViewBag.metaDescription = thispage.Description;
                ViewBag.metaKeywords = thispage.Keywords;
                ViewBag.metaAuthor = thispage.og_admins;
                ViewBag.metaType = thispage.og_type;

                string contentFilePath = Server.MapPath("/ContentPages/") + thispage.Filename + ".inc";
                ViewBag.contentFilePath = contentFilePath;
                ViewBag.subcontent_type = thispage.SubPageType;


                string pageTypeNames = cms_PagesType.Fetch("").Where(x => x.PageTypeID == thispage.PageTypeID).FirstOrDefault().PageType;

                if (pageTypeNames == "normal" || pageTypeNames=="home")
                {
                    pageName = thispage.Filename;
                }
                else if (pageTypeNames == "redirect" || pageTypeNames == "externalLink" || pageTypeNames == "link")
                {
                    string[] link = thispage.LinkAddress.Split('/');

                    if (link.Length == 3)
                    {
                        return RedirectToAction(link[2].ToString(), link[1].ToString());
                    }
                    else if (link.Length == 2)
                    {
                        pageName = link[1].ToString();
                    }
                }
                else
                {
                    pageName = string.Empty;
                }

                if (System.IO.File.Exists(contentFilePath))
                {
                    ViewBag.content = System.IO.File.ReadAllText(contentFilePath);
                }
                if (thispage.SubPageFilename != null && thispage.SubPageType == "file")
                {
                    string subContentFilePath = Server.MapPath("/ContentPages/") + thispage.SubPageFilename + ".inc";
                    ViewBag.subContentfile = thispage.SubPageFilename;
                    ViewBag.subContent = System.IO.File.ReadAllText(subContentFilePath);
                }
                else if (thispage.SubPageType == "content")
                {
                    string subContentFilePath = Server.MapPath("/ContentPages/") + thispage.Filename + "_alt" + ".inc";
                    ViewBag.subContentFile = thispage.Filename + "_alt";
                    if (System.IO.File.Exists(subContentFilePath))
                    {
                        ViewBag.subContent = System.IO.File.ReadAllText(subContentFilePath);
                    }
                    else
                    {
                        ViewBag.subContent = string.Empty;
                        ViewBag.subContentFile = string.Empty;
                    }
                }
                else
                {
                    ViewBag.subContent = string.Empty;
                }

                string footerContentFilePath = Server.MapPath("/ContentPages/") + thispage.Filename + "_bottomcontent" + ".inc";
                ViewBag.footerContentFile = thispage.Filename + "_bottomcontent";
                if (System.IO.File.Exists(footerContentFilePath))
                {
                    ViewBag.footerContent = System.IO.File.ReadAllText(footerContentFilePath);
                }
                else
                {
                    ViewBag.footerContent = string.Empty;
                    ViewBag.footerContentFile = string.Empty;
                }

            }
            else
            {
                if (cleanUrl != "default")
                {
                    Response.Status = "404 Not Found";
                    ViewBag.Message = "Page not found.";
                    ViewBag.pageName = "404";
                    ViewBag.Editable = false;
                }
            }




            ViewBag.Editable = false;
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                if (User.IsInRole("Admin")) //|| DepartmentUser.Fetch("Where UserId=@0 and DepartmentName=@1 and IsAssigned=@2", user.Id, pageName, true).Any())
                {
                    ViewBag.Editable = true;
                }
            }



            try {
                if (System.IO.File.Exists(Server.MapPath("/ContentPages/social_media.inc")))
                    ViewBag.socialmedia_Content = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/social_media.inc"));
            }
            catch { }
            try
            {
                if (System.IO.File.Exists(Server.MapPath("/ContentPages/footer_contact.inc")))
                    ViewBag.footer_contact = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/footer_contact.inc"));
            }
            catch { }
            try
            {

                List<Event> events = Event.Fetch("WHERE Active = 1 AND Featured = 1");
                ViewBag.Events = events;
            }
            catch { }
            try
            {

                List<NewsItem> newsitems = NewsItem.Fetch("").OrderByDescending(x => x.NewsItem_ID).ToList();
                ViewBag.NewsItems = newsitems;
            }
            catch { }
            try
            {

                List<SlideShowImage> getSlideshows = DefaultConnectionDB.GetInstance().Fetch<SlideShowImage>(PetaPoco.Sql.Builder
                  .Append("SELECT SlideShowImages.* from [SlideShowImages]")
                  .Append("RIGHT JOIN SlideShows ON SlideShows.SlideShowID = [SlideShowImages].SlideShowID Where  SlideShows.Active = 1")).ToList();
                ViewBag.Slideshowimages = getSlideshows;
            }
            catch { }

            if (cleanUrl.ToLower() == "default")
            {

                return View("Index");
            }
            
            return View("ShowContent");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        [LoadActionFilters]
        public ActionResult Contact()
        {
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];
            var selfpage = allpages.FirstOrDefault(x => String.Equals(x.LinkAddress, "/Home/Contact", StringComparison.OrdinalIgnoreCase));
            ViewBag.PageID = selfpage.PageID;
            ViewBag.ParentPage = selfpage.ParentPage;

            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableContact'").Value;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Contact is disabled.");
            }

            string contentFilePath = Server.MapPath("/ContentPages/") + "contact_page.inc";
            ViewBag.contentFilePath = contentFilePath;
            if (System.IO.File.Exists(contentFilePath))
            {
                ViewBag.content = System.IO.File.ReadAllText(contentFilePath);
            }

            string subContentFilePath = Server.MapPath("/ContentPages/") + "Contact_alt.inc";
            ViewBag.subContentfile = subContentFilePath;
            if (System.IO.File.Exists(subContentFilePath))
            {
                ViewBag.subcontent = System.IO.File.ReadAllText(subContentFilePath);
                ViewBag.subcontent_type = "text";
            }

            ViewBag.Editable = false;
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                if (User.IsInRole("Admin")) //|| DepartmentUser.Fetch("Where UserId=@0 and DepartmentName=@1 and IsAssigned=@2", user.Id, pageName, true).Any())
                {
                    ViewBag.Editable = true;
                }
            }
            ViewBag.Message = "Your contact page.";
            var captchaEnabled = configuration_setting.SingleOrDefault("WHERE [Key] = 'UseCaptcha'").Value;
            if (captchaEnabled.Equals("True") || captchaEnabled.Equals("1"))
            {
                ViewBag.useCaptcha = "recaptcha";
            }
            PopulateDeptEmailDropDownList();

            return View();
        }
        [HttpPost]
        public ActionResult Contact(ContactDetails contact, FormCollection formEmail)
        {
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];
            var selfpage = allpages.FirstOrDefault(x => String.Equals(x.LinkAddress, "/Home/Contact", StringComparison.OrdinalIgnoreCase));
            ViewBag.PageID = selfpage.PageID;
            ViewBag.ParentPage = selfpage.ParentPage;

            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableContact'").Value;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Contact is disabled.");
            }

            string contentFilePath = Server.MapPath("/ContentPages/") + "contact_page.inc";
            ViewBag.contentFilePath = contentFilePath;
            if (System.IO.File.Exists(contentFilePath))
            {
                ViewBag.content = System.IO.File.ReadAllText(contentFilePath);
            }

            ViewBag.Editable = false;
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                if (User.IsInRole("Admin")) //|| DepartmentUser.Fetch("Where UserId=@0 and DepartmentName=@1 and IsAssigned=@2", user.Id, pageName, true).Any())
                {
                    ViewBag.Editable = true;
                }
            }

            if (ModelState.IsValid)
            {
                var captchaEnabled = configuration_setting.SingleOrDefault("WHERE [Key] = 'UseCaptcha'").Value;
                if (captchaEnabled.Equals("True") || captchaEnabled.Equals("1"))
                {
                    ViewBag.useCaptcha = "recaptcha";
                    RecaptchaVerificationHelper recaptchaHelper = this.GetRecaptchaVerificationHelper();

                    if (String.IsNullOrEmpty(recaptchaHelper.Response))
                    {
                        ModelState.AddModelError("", "You must complete the captcha.");
                        PopulateDeptEmailDropDownList();
                        return View();
                    }
                    RecaptchaVerificationResult recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

                    if (recaptchaResult != RecaptchaVerificationResult.Success)
                    {
                        ModelState.AddModelError("", "Invalid captcha response.");
                        PopulateDeptEmailDropDownList();
                        return View();
                    }
                }
                ContactSubmission objcontact = new ContactSubmission();
                objcontact.FirstName = contact.Firstname;
                objcontact.LastName = contact.Lastname;

                objcontact.Email = contact.Email;
                //var deptId = formEmail["DeptEmail"];
                var deptId = contact.Department;

                //string departmentEmailAddress = Department.Fetch("").Where(x => x.DepartmentTypeId == Convert.ToInt32(deptId)).Select(y => y.DepartmentEmailAddress).SingleOrDefault();
                string department = Department.Fetch("").Where(x => x.DepartmentTypeId == Convert.ToInt32(deptId)).Select(y => y.DepartmentName).SingleOrDefault();
                objcontact.Department = department;
                /*
                if (!string.IsNullOrEmpty(departmentEmailAddress))
                {
                    objcontact.Email = departmentEmailAddress;
                }
                else
                {
                    PopulateDeptEmailDropDownList();
                    return View();
                }
                */
               
                objcontact.Phone = contact.Phone;
                objcontact.Company = contact.Company;
                objcontact.Address = contact.Address;
                objcontact.City = contact.City;
                objcontact.State = contact.State;
                objcontact.ZIP = contact.ZIP;
                objcontact.IP = contact.IP;
                objcontact.ContactDateTime = DateTime.Now.Date;
                objcontact.Comments = contact.Comments;
                objcontact.Save();


                // send the notification email with the contact details to the admin if configured
                if (!String.IsNullOrEmpty(GlobalSettings.config_settings["SendContactDetail"]))
                {
                    ViewBag.layout4email = true;
                    //String mail_body = RenderViewToString(this.ControllerContext, "~/Views/Home/Contact.cshtml", objcontact, true);
                    String mail_body ="";
                    String field_format = "<b>{0}</b>: {1}<br/>";
                    mail_body += String.Format(field_format, "First name", Server.HtmlEncode(contact.Firstname));
                    mail_body += String.Format(field_format, "Last name", Server.HtmlEncode(contact.Lastname));
                    mail_body += String.Format(field_format, "Phone", Server.HtmlEncode(contact.Phone));
                    mail_body += String.Format(field_format, "City", Server.HtmlEncode(contact.City));
                    mail_body += String.Format(field_format, "Email", Server.HtmlEncode(contact.Email));
                    mail_body += String.Format(field_format, "Date", DateTime.Now);
                    mail_body += String.Format(field_format, "Comments", Server.HtmlEncode(contact.Comments));

                    SendEmail.Send(GlobalSettings.config_settings["ContactEmailTo"], mail_body, GlobalSettings.config_settings["ContactEmailSubject"], GlobalSettings.config_settings["ContactEmailFrom"], "");
                }



                //send the thank you email if configured
                if (GlobalSettings.config_settings["SendContactThankyou"].ToBoolean())
                {
                    string To = GlobalSettings.config_settings["ToAddress"];
                    string From = GlobalSettings.config_settings["SMTPDefaultSender"];
                    string subject = GlobalSettings.config_settings["ContactThankYouSubject"] ?? "Thank You for Your Inquiry";
                    string body = string.Empty;
                    //Read template file from the Helpers folder
                    using (var sr = new StreamReader(Server.MapPath("~/Helpers/MailTemplates/").ToString() + "Inquiry_Thankyou.txt"))
                    {
                        body = sr.ReadToEnd();
                    }
                    string messageBody = string.Format(body, contact.Firstname);

                    //Call send email methods.
                    EmailManager.SendEmail(From, subject, messageBody, To);
                }
                return View("Contact-Thank-You");

            }
            else
            {               
                PopulateDeptEmailDropDownList();
                return View();
            }
        }
        [LoadActionFilters]
        public ActionResult DrivingDirections()
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableLocations"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Directions are disabled.");
            }

            ViewBag.Editable = false;
            return View();
        }
        public ActionResult GetDrivingDirections()
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableLocations"].ToBoolean();
            if (!enabledInConfig)
            {
                throw new HttpException(403, "Directions are disabled.");
            }

            CompanyLocation location = CompanyLocation.Fetch("ORDER BY NAME").Where(x => x.Active).FirstOrDefault();
            return Json(new { location = location }, JsonRequestBehavior.AllowGet);
        }
        [LoadActionFilters]
        public ActionResult DocumentCenter(int id = 0)
        {

            Biznetix.BizCMS.Models.DocumentCenter docCenter = new Biznetix.BizCMS.Models.DocumentCenter();
            docCenter.Folders = FST_Folder.Fetch("").Where(x => (bool)x.Active).ToList();
            if (id != 0)
            {
                ViewBag.FolderId = id;
                docCenter.Files = FST_File.Fetch("").Where(x => (bool)x.Active && x.FolderID == id).ToList();
            }
            else
            {
                docCenter.Files = null;
                ViewBag.FolderId = -1;
            }
            return View(docCenter);
        }
        [LoadActionFilters]
        public ActionResult DocumentFiles()
        {

            return View();
        }
        public void Upload(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/ContentPages/Editoruploads"), ImageName);
                upload.SaveAs(path);
            }
        }
        public void LoadBottomContent()
        {

        }
        public ActionResult LookupTags(string term)
        {
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> navpages = cms_Page.Fetch("WHERE Display<>0 ORDER BY sort");
            var result = new List<KeyValuePair<string, string>>();
            foreach (var item in navpages)
            {
                result.Add(new KeyValuePair<string, string>(item.PageID.ToString(), item.PageName));
            }

            var retValue = result.Where(s => s.Value.ToLower().Contains(term.ToLower())).Select(w => w).ToList();

            return Json(retValue, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LookupFilename(int id)
        {
            string filename = cms_Page.Fetch("WHERE PageID=@0", id).Select(x => x.Filename).FirstOrDefault();
            return Json(filename, JsonRequestBehavior.AllowGet);
        }

        private void PopulateDeptEmailDropDownList()
        {
            Department dept;
            dept = new Department();
            dept.DepartmentTypeId = 1;
            var departments = from t in Department.Fetch("")
                               orderby t.DepartmentName
                               select t;

            ViewBag.Department = new SelectList(departments, "DepartmentTypeId", "DepartmentName", dept.DepartmentTypeId);
        }
        static string RenderViewToString(ControllerContext context,
                        string viewPath,
                        object model = null,
                        bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }
    }
}