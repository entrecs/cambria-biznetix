﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Models;

namespace Biznetix.BizCMS.Controllers
{
    public class DonateController : Controller
    {
        // GET: Donate
        public ActionResult Index()
        {
            string payPalClientId;
            payPalClientId = ConfigurationManager.AppSettings["payPalClientId"];

            ViewBag.payPalSmartButtonUrl = $@"https://www.paypal.com/sdk/js?client-id={payPalClientId}&disable-funding=credit,card";
            ViewBag.emailValidationRegex = @"^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";


            return View();
        }

        [HttpPost]
        public ActionResult DonationCompleted(DonationReceipt donationReceipt)
        {
            // TODO: Save the contact submission, fetch the id.
            try
            {
                donationReceipt.Save();
            }
            catch (Exception ex) {
                return Json(new { success = false, message = ex.Message });
            }

            return Json(new { success = true });
        }

        public ActionResult ThankYou()
        {
            return View();
        }

    }
}