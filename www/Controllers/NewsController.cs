﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Controllers
{
    public class NewsController : Controller
    {
         [LoadActionFilters]
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "News is disabled.");
            }
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> navpages = cms_Page.Fetch("WHERE Display<>0 ORDER BY sort");
            ViewBag.navpages = navpages;
            List<NewsItem> newsitems = NewsItem.Fetch("Where [active]<>0 and ExpiresDate > '" + DateTime.Today + "' ORDER BY PostedDate");
            ViewBag.NewsItems = newsitems;
            if (newsitems != null)
            {
                return View(newsitems);
            }
            return View();
        }
         [LoadActionFilters]
        public ActionResult Article(int id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "News is disabled.");
            }
            List<NewsItem> newsitems = NewsItem.Fetch("");         
            NewsItem newsitem = NewsItem.FirstOrDefault("WHERE NewsItem_ID = @0", id);                   
            if (newsitem != null)
            {
                
                return View(newsitem);
            }

            return View();
        }
         [LoadActionFilters]
         public PartialViewResult Headlines()
         {
            List<NewsItem> newsitems = NewsItem.Fetch("");            
            ViewBag.NewsItems = newsitems;
            
            if (newsitems != null)
            {
                return PartialView( newsitems);
            }
            return PartialView();
         }
    }
}