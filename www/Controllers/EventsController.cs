﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using Biznetix.BizCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.Common.StringExtensions;

namespace Biznetix.BizCMS.Controllers
{
    public class EventsController : Controller
    {
        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        [LoadActionFilters]
        public ActionResult Index()
        {
            
            if (!GlobalSettings.config_settings["EnableEvents"].ToBoolean() )
            {
                throw new HttpException(403, "Events are disabled.");
            }

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];
            var selfpage = allpages.FirstOrDefault(x => String.Equals(x.LinkAddress, "/Events/Index", StringComparison.OrdinalIgnoreCase));
            ViewBag.PageID = selfpage.PageID;
            ViewBag.ParentPage = selfpage.ParentPage;


            List<Biznetix.BizCMS.Data.Event> events = new List<Event>();

            foreach (Event et in ApplicationCache.EventsCache)
            {
                if (et.Active == true)
                {
                    events.Add(et);
                }
            }
            ApplicationCache.AddToCache("Events", events);

            ViewBag.SubmissionsEnabled = GlobalSettings.config_settings["EnableEventSubmissions"].ToBoolean();

            return View("Month", ApplicationCache.EventsCache);
        }
        [LoadActionFilters]
        [HttpGet]
        public ActionResult Lists(DateTime start, DateTime end)
        {
            if (!GlobalSettings.config_settings["EnableEvents"].ToBoolean())
            {
                throw new HttpException(403, "Events are disabled.");
            }
            List<Event> events = Event.Fetch("WHERE EventStartDate>=@0 AND EventEndDate<=@1 AND Active=1", start, end);
            var json = events.Select(x => new
            {
                EventID = x.EventID,
                title = x.Title,
                description = x.Description,
                start = x.EventStartDate.Value.ToShortDateString(),
                end = x.EventEndDate.Value.AddDays(1).ToShortDateString()
            });
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [LoadActionFilters]
        public ActionResult EventDetail(int id)
        {
            if (!GlobalSettings.config_settings["EnableEvents"].ToBoolean())
            {
                throw new HttpException(403, "Events are disabled.");
            }

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];
            ViewBag.PageID = allpages.FirstOrDefault(x => String.Equals(x.LinkAddress, "/Events/Index", StringComparison.OrdinalIgnoreCase)).PageID;

            Event eventdetail = Event.Single("WHERE EventID=@0", id);
            return View("EventDetail", eventdetail);
        }

        [Authorize]
        public ActionResult SubmitNew()
        {
            if (!GlobalSettings.config_settings["EnableEvents"].ToBoolean()  || !GlobalSettings.config_settings["EnableEventSubmissions"].ToBoolean())
            {
                throw new HttpException(403, "Event submissions are disabled.");
            }
            
            return View("SubmitNew");
        }


        [HttpPost]
        [Authorize]
        public ActionResult SubmitNew([Bind(Exclude = "EventID")]Event newEvent)
        {
            if (!GlobalSettings.config_settings["EnableEvents"].ToBoolean() || !GlobalSettings.config_settings["EnableEventSubmissions"].ToBoolean())
            {
                throw new HttpException(403, "Event submissions are disabled.");
            }

            if (ModelState.IsValid)
            {
                // Add event to db
                var eventId = DefaultConnectionDB.GetInstance().Insert("Events", "EventID", newEvent);

                // Notify staff
                string to = configuration_setting.SingleOrDefault("WHERE [Key] = 'FormEmailTo'").Value;
                string from = configuration_setting.SingleOrDefault("WHERE [Key] = 'FormEmailFrom'").Value;
                string subject = "Event Submission";
                var url = Url.RequestContext.HttpContext.Request.Url; // TODO: Is there a nicer way to get an absolute url for a specific route?
                string link = url.Scheme + "://" + url.Authority + "/BizAdmin/Events/Edit/" + eventId.ToString();
                string body = String.Format("A new event has been submitted by a user. Click <a href=\"{0}\">here</a> to review the submitted event \"{1}\".", link, HttpUtility.HtmlEncode(newEvent.Title));
                SendEmail.Send(to, body, subject, from, "");
                return Redirect("/form-submitted");
            }
            else
            {
                return View();
            }

        }

        public ActionResult Register(int id)
        {
            Event eventDetail = Event.Single(id);
            if (eventDetail!=null)
                ViewBag.EventDetail = eventDetail;

            return View("EventRegistration");
        

        }

        [HttpPost]
        [Authorize]
        public ActionResult Register(EventRegistration registration)
        {
            Event eventDetail = Event.Single(registration.EventID);
            if (eventDetail != null)
                ViewBag.EventDetail = eventDetail;

            registration.Insert();

            return View();


        }

    }
}