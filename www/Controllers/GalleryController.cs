﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Controllers
{
    public class GalleryController : Controller
    {
        // GET: Gallery
        [LoadActionFilters]
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Galleries are disabled.");
            }

            try { ViewBag.footer_social = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "social_media.inc"); }
            catch { }
            try { ViewBag.footer_contact = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "footer_contact.inc"); }
            catch { }
            
            

            ViewBag.Editable = false;   
            List<MediaGalleryCategory> galleries = DefaultConnectionDB.GetInstance().Fetch<MediaGalleryCategory>(PetaPoco.Sql.Builder
                .Append("SELECT MediaGalleryCategories.*, ImagePath as PrimaryImageUrl")
                .Append("FROM MediaGalleryCategories LEFT JOIN MediaGalleryImages ON MediaGalleryCategories.FrontImageId=MediaGalleryImages.ImageId ORDER BY SORT"));           
            return View("Index", galleries);
        }
        public ActionResult DisplayGalleryById(int idnum)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Galleries are disabled.");
            }

            var gallery = MediaGalleryCategory.Single(idnum);

            ViewBag.Title = gallery.CategoryName;
            ViewBag.GalleryTitle = gallery.CategoryName;
            
            List<MediaGalleryImage> images = MediaGalleryImage.Fetch("WHERE CategoryId=@0 ORDER BY SORT", idnum);
            return View("DisplayGallery", images);
        }
        [LoadActionFilters]
        public ActionResult DisplayGallery(string id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Galleries are disabled.");
            }
            if (string.IsNullOrEmpty(id))
            {
                //throw new HttpException(404, "Gallery not found");
                return new HttpNotFoundResult();
            }
            string categoryName = id;

            try { ViewBag.footer_social = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "social_media.inc"); }
            catch { }
            try { ViewBag.footer_contact = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "footer_contact.inc"); }
            catch { }
            ViewBag.Editable = false;   
            if (string.IsNullOrEmpty(categoryName))
            {
                //throw new HttpException(404, "Gallery not found");
                return new HttpNotFoundResult();
            }            
            var gallery = MediaGalleryCategory.SingleOrDefault("WHERE cleanurl = @0", categoryName);
            if (gallery != null)
            {
                ViewBag.Title = gallery.CategoryName;
                ViewBag.GalleryTitle = gallery.CategoryName;
                var galleryid = gallery.CategoryID;

                List<MediaGalleryImage> images = MediaGalleryImage.Fetch("WHERE CategoryId=@0 ORDER BY SORT", galleryid);
                return View("DisplayGallery", images);
            }
            else
            {
                List<MediaGalleryCategory> galleries = MediaGalleryCategory.Fetch("");
                return View("Index", galleries);
            }

        }



        [LoadActionFilters]
        public ActionResult DisplayGallery2(string id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                throw new HttpException(403, "Galleries are disabled.");
            }
            if (string.IsNullOrEmpty(id))
            {
                //throw new HttpException(404, "Gallery not found");
                return new HttpNotFoundResult();
            }
            string categoryName = id;

            try { ViewBag.footer_social = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "social_media.inc"); }
            catch { }
            try { ViewBag.footer_contact = System.IO.File.ReadAllText(Server.MapPath("/ContentPages/") + "footer_contact.inc"); }
            catch { }
            ViewBag.Editable = false;
            if (string.IsNullOrEmpty(categoryName))
            {
                //throw new HttpException(404, "Gallery not found");
                return new HttpNotFoundResult();
            }
            var gallery = MediaGalleryCategory.SingleOrDefault("WHERE cleanurl = @0", categoryName);
            if (gallery != null)
            {
                ViewBag.Title = gallery.CategoryName;
                ViewBag.GalleryTitle = gallery.CategoryName;
                var galleryid = gallery.CategoryID;

                List<MediaGalleryImage> images = MediaGalleryImage.Fetch("WHERE CategoryId=@0 ORDER BY SORT", galleryid);
                return View("DisplayGallery2", images);
            }
            else
            {
                List<MediaGalleryCategory> galleries = MediaGalleryCategory.Fetch("");
                return View("Index", galleries);
            }

        }



        public ActionResult ListGalleries()
        {
            List<MediaGalleryCategory> galleries = DefaultConnectionDB.GetInstance().Fetch<MediaGalleryCategory>(PetaPoco.Sql.Builder
            .Append("SELECT MediaGalleryCategories.*, ImagePath as PrimaryImageUrl")
            .Append("FROM MediaGalleryCategories LEFT JOIN MediaGalleryImages ON MediaGalleryCategories.FrontImageId=MediaGalleryImages.ImageId ORDER BY SORT"));
            return View("ListGalleries", galleries);
        }
    }
}