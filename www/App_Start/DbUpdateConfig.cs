﻿using DbUp;
using DbUp.Engine.Output;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Biznetix.BizCMS
{
    public class StreamWriterUpdateLog : IUpgradeLog, IDisposable
    {
        StreamWriter _stream;
        StringBuilder _log = new StringBuilder();

        public string Log
        {
            get { return _log.ToString(); }
        }

        public void SetStream(StreamWriter stream)
        {
            if (_stream != stream)
            {
                _stream = stream;
                // write any info logged before we set the stream
                _stream.Write(this.Log);
            }
        }

        public void Dispose()
        {
            if (_stream != null) _stream.Dispose();
            _stream = null;
        }

        public void WriteMessage(string format, string type, params object[] args)
        {
            string message = string.Format(format, args);
            message = string.Format("[{0}]\t{1}\t\t{2}", DateTime.Now, type, message);

            // write to the in memory log buffer
            _log.AppendLine(message);

            // write to the stream if it exists
            if (_stream != null)
            {
                _stream.WriteLine(message);
            }
        }

        public void WriteError(string format, params object[] args)
        {
            WriteMessage(format, "ERROR", args);
        }

        public void WriteInformation(string format, params object[] args)
        {
            WriteMessage(format, "INFO", args);
        }

        public void WriteWarning(string format, params object[] args)
        {
            WriteMessage(format, "WARN", args);
        }
    }

    public class DbUpdateConfig
    {
        public static void ExecuteScripts()
        {
            // this could probably be done more elegantly...
            var logger = new StreamWriterUpdateLog();

            var upgrader = DeployChanges.To
                .SqlDatabase(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
                .WithScriptsFromFileSystem(HttpContext.Current.Server.MapPath("~/Scripts/DbUp"))
                .LogTo(logger)
                .WithTransactionPerScript()
                .Build();

            if (upgrader.IsUpgradeRequired())
            {
                // don't create the stream until after we determined upgrade, otherwise we'd be writing constant "no updates required" spam
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"~/log"));
                using (StreamWriter writer = new StreamWriter(HttpContext.Current.Server.MapPath($"~/log/DatabaseUpgrade.{DateTime.Now:yyyyMMdd}.log")))
                {
                    // set our logger's stream
                    logger.SetStream(writer);

                    var result = upgrader.PerformUpgrade();

                    if (!result.Successful)
                    {
                        // TODO: redirect to a friendly screen... email entre?
                        throw new SystemException("An error occurred during database upgrade - " + result.Error.Message, result.Error);
                    }
                }
            }
        }
    }
}