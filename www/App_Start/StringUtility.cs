﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Specialized;


namespace Biznetix.Common.StringExtensions
{
    /// <summary>
    /// Summary description for StringUtility
    /// </summary>
    public static class StringUtility
    {
        public const string WhitelistHTMLRegexPattern = @"\<((?=(?!\b(a|b|i|p|u|div|table|th|tr|td|br|hr|img|article|audio|bdo|blockquote|caption|code|col|colgroup|dd|dfn|dl|dt|em|h1|h2|h3|h4|h5|h6|label|li|ol|picture|pre|q|s|section|small|source|span|strong|style|sub|sup|tbody|tfoot|thead|ul|video|wbr)\b))(?=(?!\/\b(a|b|i|p|u|div|table|th|tr|td|br|hr|img|article|audio|bdo|blockquote|caption|code|col|colgroup|dd|dfn|dl|dt|em|h1|h2|h3|h4|h5|h6|label|li|ol|picture|pre|q|s|section|small|source|span|strong|style|sub|sup|tbody|tfoot|thead|ul|video|wbr)\b))).*?\>";
        public static string SanitizeHTML(this string htmlString)
        {

            if (!string.IsNullOrEmpty(htmlString))
            {
                return Regex.Replace(htmlString, WhitelistHTMLRegexPattern, "", RegexOptions.IgnoreCase, new TimeSpan(0, 0, 0, 0, 250));
            }
            return null;

        }

        public static string StripHTML(this string htmlString)
        {
            if (!string.IsNullOrEmpty(htmlString))
            {
                return Regex.Replace(htmlString, @"<style(.|\n)*?<\/style>|<script(.|\n)*?<\/script>|<(.|\n)*?>", string.Empty).Replace("&nbsp;", " ");
            }
            return null;
        }

        public static string StripHTMLScripts(this string htmlString)
        {

            return Regex.Replace(htmlString, @"<script(.|\n)*?<\/script>", string.Empty).Replace("&nbsp;", " ");
        }

        public static string StripHTMLStyle(this string htmlString)
        {

            return Regex.Replace(htmlString, @"<style(.|\n)*?<\/style>", string.Empty).Replace("&nbsp;", " ");
        }

        public static string ReplaceSpecialChar(this string str)
        {
            return Regex.Replace(str.Trim(), "[^a-zA-Z0-9]+", "");
        }

        public static string ReplaceSpecialChar(this string str, string replaceWith)
        {
            return Regex.Replace(str.Trim(), "[^a-zA-Z0-9]+", replaceWith);
        }

        public static string SetQSParameter(this Uri url, string key, string value)
        {
            var query = HttpUtility.ParseQueryString(url.Query);
            query[key] = value; // Add or replace param

            return url.AbsolutePath + "?" + query;
        }

        public static IEnumerable<T> SkipLastN<T>(this IEnumerable<T> source, int n)
        {
            var it = source.GetEnumerator();
            bool hasRemainingItems = false; 
            var cache = new Queue<T>(n + 1); 
            
            do
            {
                if (hasRemainingItems = it.MoveNext())
                { 
                    cache.Enqueue(it.Current); 
                    if (cache.Count > n)
                        yield return cache.Dequeue();
                }
            } 
            while (hasRemainingItems);
        }
        public static bool ToBoolean(this string value)
        {
            switch (value.ToLower())
            {
                case "true":
                    return true;
                case "yes":
                    return true;
                case "t":
                    return true;
                case "1":
                    return true;
                case "0":
                    return false;
                case "false":
                    return false;
                case "f":
                    return false;
                default:
                    throw new InvalidCastException("You can't cast that value to a bool!");
            }
        }

        public static string toQueryStringValue(this IEnumerable<string> list_string)
        {
            StringBuilder qsValue = new StringBuilder();

            foreach (string s in list_string.SkipLastN(1))
                qsValue.Append(s + ",");

            qsValue.Append(list_string.Last());
            return qsValue.ToString();
        }

        // for generic interface IEnumerable<T>
        public static string ToString<T>(this IEnumerable<T> source, string separator)
        {
            if (source == null)
                throw new ArgumentException("Parameter source can not be null.");

            if (string.IsNullOrEmpty(separator))
                throw new ArgumentException("Parameter separator can not be null or empty.");

            string[] array = source.Where(n => n != null).Select(n => n.ToString()).ToArray();

            return string.Join(separator, array);
        }

        public static string Excerpt(this string inputString, int minLength, int maxLength)
        {

            string theString, PunctuationString;
            int i = 0, orginal_length;

            if (string.IsNullOrEmpty(inputString)) return null;

            theString = inputString.StripHTML().Trim();
            orginal_length = theString.Length;

            if ((theString.Length <= maxLength) || string.IsNullOrEmpty(theString)) return theString;

            PunctuationString = ".!?:;, ";

            foreach (char a in PunctuationString)
            {
                i = theString.LastIndexOf(a, maxLength, maxLength - minLength);
                if ((i > 0) && (i < maxLength) && i >= minLength) maxLength = i;
            }

            theString = theString.Substring(0, maxLength);

            if (orginal_length > theString.Length) theString += "...";

            return theString;


        }

        public static int intSafe(this string inputString, int default_value)
        {
            int _parsedValue;

            if (string.IsNullOrEmpty(inputString)) return default_value;
            return (int.TryParse(inputString, out _parsedValue)) ? _parsedValue : default_value;

        }

        public static int? intSafe(this string inputString, int? default_value)
        {
            int _parsedValue;

            if (string.IsNullOrEmpty(inputString)) return default_value;
            return (int.TryParse(inputString, out _parsedValue)) ? _parsedValue : default_value;

        }

        public static decimal decSafe(this string inputString, decimal default_value)
        {
            decimal _parsedValue;

            if (string.IsNullOrEmpty(inputString)) return default_value;
            return (decimal.TryParse(inputString, out _parsedValue)) ? _parsedValue : default_value;

        }

        public static string Replace(this string formattext, Dictionary<string, string> replacements)
        {
            return new Regex(@"\!\*(?<key>([^\*\r\n]+))\*\!").Replace(formattext,
                        match => replacements.ContainsKey((match.Groups[1].Captures.Count > 0 ? match.Groups[1].Captures[0].Value :
                            match.Groups[2].Captures[0].Value)) ? replacements[(match.Groups[1].Captures.Count > 0 ? match.Groups[1].Captures[0].Value :
                            match.Groups[2].Captures[0].Value)] : match.ToString());
            // (\!\*<key>\*\!)
            // "\{(?<key>\w+)\}"
        }
        public static string Replace(this string formattext, NameValueCollection replacements)
        {
            return
                   new Regex(@"\!\*(?<key>([^\*\r\n]+))\*\!").Replace(formattext,
                        match => replacements[(match.Groups[1].Captures.Count > 0 ? match.Groups[1].Captures[0].Value :
                            match.Groups[2].Captures[0].Value)] ?? match.ToString());


            //        return new Regex(@"\!\*(?<key>\w+)\*\!").Replace(formattext,
            //             match => replacements[match.Groups[1].Captures[0].Value]);

        }
        public static bool IsValidEmail(this string inputString)
        {

            //  string email_pattern = @"^[\\w-_\.]*[\\w-_\.]\@[\\w]\.+[\\w]+[\\w]$";
            //  string email_pattern = @"^[\w-_\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$" ;
            //    string email_pattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            string email_pattern = @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$";
            return Regex.IsMatch(inputString, email_pattern);

        }

    }
}


