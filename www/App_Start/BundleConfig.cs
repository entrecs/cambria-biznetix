﻿using System.Web;
using System.Web.Optimization;

namespace Biznetix.BizCMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.UseCdn = true;
            //BundleTable.EnableOptimizations = true;

            var jqueryCdnPath = "https://code.jquery.com/jquery-3.4.1.min.js";
            var jqueryBundle = new ScriptBundle("~/bundles/jquery", jqueryCdnPath).Include("~/Scripts/jquery-{version}.js");
            jqueryBundle.CdnFallbackExpression = "window.jQuery";
            bundles.Add(jqueryBundle);

            var jquerymigrateCdnPath = "https://code.jquery.com/jquery-migrate-1.4.1.min.js";
            var jquerymigrateBundle = new ScriptBundle("~/bundles/jquerymigrate", jquerymigrateCdnPath).Include("~/Scripts/jquery-migrate-{version}.js");
            jquerymigrateBundle.CdnFallbackExpression = "jQuery.migrateVersion";
            bundles.Add(jquerymigrateBundle);

            bundles.Add(new ScriptBundle("~/bundles/jqueryaddins").Include(
                    "~/Scripts/jquery.easing.1.3.js",
                    "~/Scripts/jquery.mobilemenu.js",
                    "~/Scripts/jquery.equalheights.js",
                    "~/Scripts/moment.js"));


            bundles.Add(new ScriptBundle("~/bundles/superfish").Include(
                "~/Scripts/superfish.js"));


            bundles.Add(new ScriptBundle("~/bundles/sForm").Include(
                    "~/Scripts/sForm.js"));

            bundles.Add(new ScriptBundle("~/bundles/camera").Include(
                    "~/Scripts/camera.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Bootstrap/bootstrap.bundle.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/css/bootstrap/bootstrap.min.css",
                      "~/Content/css/site.css")
                      .Include("~/Content/css/Font Awesome/fontawesome.min.css", new CssRewriteUrlTransform())
                      .Include("~/Content/css/Font Awesome/fa-regular.min.css", new CssRewriteUrlTransform())
                      .Include("~/Content/css/Font Awesome/fa-solid.min.css", new CssRewriteUrlTransform())
                      .Include("~/Content/css/Font Awesome/fa-brands.min.css", new CssRewriteUrlTransform()));
                      //"~/Content/bootstrap.css",
                      //"~/Content/css/responsive.css",
                      //"~/Content/style.css",
                      //"~/Content/css/camera.css"));
                      //"~/Content/font/font-awesome.css"

            bundles.Add(new ScriptBundle("~/bundles/jCarouselLite").Include(
             "~/Scripts/jcarousellite.min.js",
             "~/Scripts/jquery.fancybox.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/imageresizeandcrop").Include(
            "~/Scripts/jquery.Jcrop.js"));

            bundles.Add(new StyleBundle("~/bundles/css/imageresizeandcrop").Include(
                                "~/Content/jquery.Jcrop.css"));

            bundles.Add(new StyleBundle("~/bundles/css/uploadify").Include(
                    "~/Content/css/uploadifive.css"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/uploadify").Include(
                    "~/Scripts/jquery.uploadifive.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/ckeditor").Include(
            "~/include/editor/ckeditor.js",
            "~/include/ckfinder/ckfinder.js"));

            bundles.Add(new StyleBundle("~/bundles/css/bizadmin").Include(
                "~/Content/css/bootstrap/bootstrap.min.css",
                "~/Content/jquery.fancybox.css",
                "~/Content/Bizadmin/bizadmin_style.css"));

            bundles.Add(new ScriptBundle("~/bundles/bizadmin/bootstrap").Include(
                  "~/Scripts/Bootstrap/bootstrap.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                
                "~/Content/DataTables-1.10.18/media/js/jquery.dataTables.js"));

            
            var datatablesBundle = new ScriptBundle("~/bundles/dataTables", "https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/datatables.min.js")
                    .Include("~/Content/dataTables.js");
            datatablesBundle.CdnFallbackExpression = "$.fn.dataTable";
            bundles.Add(datatablesBundle);


            bundles.Add(new StyleBundle("~/bundles/css/dataTables", "https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.6/datatables.min.css").Include(
                "~/Content/dataTables.css"));
            


            

                    bundles.Add(new StyleBundle("~/bundles/css/sortable").Include(
                "~/Content/css/sortable.css"));
            bundles.Add(new ScriptBundle("~/bundles/sortable").Include(
                "~/Scripts/sortable.js"));

            

        }
    }
}
