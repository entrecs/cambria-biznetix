﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Biznetix.BizCMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "RootContent",
                "{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new { controller = "Home" });


            routes.MapRoute(
                "PageContent",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "Biznetix.BizCMS.Controllers" });

            routes.MapRoute(
                  "Default",
                  "{controller}/{action}/{id}",
                  new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                  new[] { "Biznetix.BizCMS.Areas.BizAdmin.Controllers" });
        }
    }
}
