﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;


namespace Biznetix.BizCMS
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public sealed class RemoveScriptAttribute : ValidationAttribute
    {
        public const string DefaultRegexPattern = @"\<((?=(?!\b(a|b|i|p|u|div|table|th|tr|td|br|hr|img|article|audio|bdo|blockquote|caption|code|col|colgroup|dd|dfn|dl|dt|em|h1|h2|h3|h4|h5|h6|label|li|ol|picture|pre|q|s|section|small|source|span|strong|style|sub|sup|tbody|tfoot|thead|ul|video|wbr)\b))(?=(?!\/\b(a|b|i|p|u|div|table|th|tr|td|br|hr|img|article|audio|bdo|blockquote|caption|code|col|colgroup|dd|dfn|dl|dt|em|h1|h2|h3|h4|h5|h6|label|li|ol|picture|pre|q|s|section|small|source|span|strong|style|sub|sup|tbody|tfoot|thead|ul|video|wbr)\b))).*?\>";

        public string RegexPattern { get; }

        public RemoveScriptAttribute(string regexPattern = null)
        {
            RegexPattern = regexPattern ?? DefaultRegexPattern;
        }

        protected override ValidationResult IsValid(object value, ValidationContext ctx)
        {
            var valueStr = value as string;
            if (valueStr != null)
            {
                var newVal = Regex.Replace(valueStr, RegexPattern, "", RegexOptions.IgnoreCase, new TimeSpan(0, 0, 0, 0, 250));

                if (newVal != valueStr)
                {
                    var prop = ctx.ObjectType.GetProperty(ctx.MemberName);
                    prop.SetValue(ctx.ObjectInstance, newVal);
                }
            }

            return null;
        }
    }
}