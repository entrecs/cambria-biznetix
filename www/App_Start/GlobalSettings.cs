﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biznetix.BizCMS
{
    public class GlobalSettings
    {
        public static void LoadSettings()
        {
            List<configuration_setting> settings = configuration_setting.Fetch("");
            config_settings.Clear();
            foreach (var config in settings)
            {
                config_settings.Add(config.Key, config.Value);
            }

        }
        public static Dictionary<string,string> config_settings = new Dictionary<string, string>(); 
    }
}