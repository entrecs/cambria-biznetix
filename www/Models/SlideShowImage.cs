﻿using System.Web.Mvc;
namespace Biznetix.BizCMS.Data
{
    public partial class SlideShowImage
    {
        [AllowHtml]
        public string RawCaption
        {
            get;
            set;
        }
    }
}