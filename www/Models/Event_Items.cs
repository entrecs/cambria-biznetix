﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Data
{
    public partial class Event
    {
        //public DateTime? EventStartTime { get; set; }
        //public DateTime? EventEndTime { get; set; }


        [RegularExpression(@"^(0?[0-9]|1[0-2]):[0-5][0-9] ?(am|pm|AM|PM)$", ErrorMessage = "Invalid Time.")]
        public string EventStartTimeValue 
        {
            get { return EventStartTime.HasValue ? EventStartTime.Value.ToString("hh:mm tt") : string.Empty; }

            set { EventStartTime = DateTime.Parse(value); }
        }

        [RegularExpression(@"^(0?[0-9]|1[0-2]):[0-5][0-9] ?(am|pm|AM|PM)$", ErrorMessage = "Invalid Time.")]
        public string EventEndTimeValue
        {
            get { return EventEndTime.HasValue ? EventEndTime.Value.ToString("hh:mm tt") : string.Empty; }

            set { EventEndTime = DateTime.Parse(value); }
        }

    }
    [MetadataTypeAttribute(typeof(Event.EventMetadata))]
    public partial class Event : DefaultConnectionDB.Record<Event>
    {
        internal sealed class EventMetadata
        {
            #region Properties
            public int EventID { get; set; }
            [Required]
            public string Title { get; set; }

#pragma warning disable SCS0017 // Request validation is disabled
            [AllowHtml,RemoveScript]
#pragma warning restore SCS0017 // Request validation is disabled
            public string Description { get; set; }

            [Required, DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
            [DisplayName("Start Date")]
            public DateTime? EventStartDate { get; set; }

            [Required, DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
            [DisplayName("End Date")]
            public DateTime? EventEndDate { get; set; }


            /*[Required]*/
            public int? CategoryID { get; set; }

            #endregion
        }
    }
}