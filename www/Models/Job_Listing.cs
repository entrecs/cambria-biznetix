﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Data
{
     [MetadataTypeAttribute(typeof(Job_Listing.Job_ListingMetadata))]
    public partial class Job_Listing : DefaultConnectionDB.Record<Job_Listing>  
    {       
        internal sealed class Job_ListingMetadata
        {
            [AllowHtml]
            public string Job_description { get; set; }
            [AllowHtml]
            public string Short_Job_Description { get; set; }
            [Required(ErrorMessage = "Title is required")]
            public string Job_title { get; set; }
            [Required(ErrorMessage = "Posted Date is required")]
            public string date_Posted { get; set; }
        }
    }
}