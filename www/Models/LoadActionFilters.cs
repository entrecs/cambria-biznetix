﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Models
{
    public class LoadActionFilters : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try { filterContext.Controller.ViewBag.socialmedia_Content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/ContentPages/") + "social_media" + ".inc"); }
            catch { }

            try { filterContext.Controller.ViewBag.footer_contact = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/ContentPages/") + "footer_contact" + ".inc"); }
            catch { }

            try { filterContext.Controller.ViewBag.popularlinks = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/ContentPages/") + "popular-links" + ".inc"); }
            catch { }

            base.OnActionExecuting(filterContext);
        }
    }
}