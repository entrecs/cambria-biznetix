﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Data
{
    [MetadataTypeAttribute(typeof(cms_Page.cms_PageMetadata))]
    public partial class cms_Page : DefaultConnectionDB.Record<cms_Page>
    {
        [AllowHtml]
        public string main_content { get; set; }

        [AllowHtml]
        public string alt_content { get; set; }
        [AllowHtml]
        public string bottom_content { get; set; }

        internal sealed class cms_PageMetadata
        {
            [AllowHtml]
            public string PageName { get; set; }
        }

    }
}