﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biznetix.BizCMS.Data
{
      [MetadataTypeAttribute(typeof(Doc_File.Doc_FileMetadata))]
    public partial class Doc_File : DefaultConnectionDB.Record<Doc_File>  
    {
          internal sealed class Doc_FileMetadata
          {
              [Required]
              public string Title { get; set; }
              [Required]
              public int FolderID { get; set; }              
              public string FileName { get; set; }
          }
    }
}