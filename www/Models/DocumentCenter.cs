﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Models
{
    public class DocumentCenter
    {
        public List<FST_Folder> Folders { get; set; }
        public List<FST_File> Files { get; set; }
    }
}