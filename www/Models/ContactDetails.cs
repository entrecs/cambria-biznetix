﻿using System.ComponentModel.DataAnnotations;

namespace Biznetix.BizCMS.Data
{
    public class ContactDetails
    {
        public int ID { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
       
        public string Email { get; set; }
        [Required]        
        public string Phone { get; set; }             
       
        public string Company { get; set; }
       
        public string Address { get; set; }
       
        public string City { get; set; }
       
        public string State { get; set; }
       
        public string ZIP { get; set; }
        //[Url]
        public string IP { get; set; }
        public string Comments { get; set; }

        public string Department { get; set; }
    }
}