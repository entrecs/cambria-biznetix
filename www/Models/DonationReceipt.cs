﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Biznetix.BizCMS.Data;
using PetaPoco;

namespace Biznetix.BizCMS.Models
{
    public class DonationReceipt
    {
        public Donation DonationDetails { get; set; }
        public ContactSubmission ContactInfo { get; set; }

        public void Save()
        {
            string saveContactInfoErrorMessage = String.Empty;
            int insertedContactId = 0;

            try
            {
                insertedContactId = (int)ContactInfo.Insert();
            }
            catch (Exception ex)
            {
                saveContactInfoErrorMessage = ex.Message;
            }

            DonationDetails.ContactId = insertedContactId > 0 ? (int?)insertedContactId : null;

            try
            {
                DonationDetails.Insert();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save donation details. " + ex.Message, ex);
            }

            if (saveContactInfoErrorMessage != String.Empty)
            {
                throw new Exception("Unable to save contact information. " + saveContactInfoErrorMessage);
            }
        }
    }
}