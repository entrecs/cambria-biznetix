﻿using Biznetix.BizCMS.Data;
using System.Web.Mvc;
using PetaPoco;

namespace Biznetix.BizCMS.Data
{
    public partial class SlideShow
    {
        [ResultColumn]
        public string PrimaryImageUrl
        {
            get;
            set;
        }
        [ResultColumn]
        public string Title
        {
            get;
            set;
        }
        [ResultColumn]
        public string Caption
        {
            get;
            set;
        }
        [AllowHtml]
        public string Description
        {
            get;
            set;
        }
    }
}