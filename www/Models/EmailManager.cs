﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Configuration;
using Biznetix.Common.StringExtensions;

namespace Biznetix.BizCMS.Models
{
    public class EmailManager
    {
        public static void AppSettings(out string UserID, out string Password, out string SMTPPort, out string Host)
        {
            UserID = ConfigurationManager.AppSettings["SMTPUser"];
            Password = ConfigurationManager.AppSettings["SMTPPassword"];
            SMTPPort = ConfigurationManager.AppSettings["SMTPPort"] ?? "25";
            Host = ConfigurationManager.AppSettings["SMTPServer"] ?? "smtp.biznetix.net";
        }

        public static void SendEmail(string From, string Subject, string Body, string To, string overrideUser = null, string overridePassword = null, string overrideHost = null, string overrideSMTPPort = null,  bool? overrideuseSSL = false)
        {
            try
            {

                string UserID = overrideUser ?? ConfigurationManager.AppSettings["SMTPUser"] ?? "developers@biznetix.net";
                string Password = overridePassword ?? ConfigurationManager.AppSettings["SMTPPassword"] ?? "";
                string SMTPPort = overrideSMTPPort ?? ConfigurationManager.AppSettings["SMTPPort"] ?? "25";
                string Host = overrideHost ?? ConfigurationManager.AppSettings["SMTPServer"] ?? "smtp.biznetix.net";
                string method = ConfigurationManager.AppSettings["SMTPMethod"] ?? "network";
                bool useSSL = overrideuseSSL ?? ConfigurationManager.AppSettings["SMTPEnableSSL"].ToBoolean();

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(To);
                mail.From = new MailAddress(From);
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                if (method.ToLower() == "pickup")
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                }
                else
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                }
                smtp.Host = Host;
                smtp.Port = Convert.ToInt16(SMTPPort);
                if (!String.IsNullOrEmpty(UserID)  && !String.IsNullOrEmpty(Password))
                {
                    smtp.Credentials = new NetworkCredential(UserID, Password);
                }
                smtp.EnableSsl = useSSL;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}