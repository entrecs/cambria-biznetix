﻿using Biznetix.BizCMS.Data;
using PetaPoco;

namespace Biznetix.BizCMS.Data
{
    public partial class MediaGalleryCategory
    {
        [ResultColumn]
        public string PrimaryImageUrl
        {
            get;
            set;
        }        
    }
}