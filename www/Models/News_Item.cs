﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biznetix.Common.StringExtensions;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace Biznetix.BizCMS.Data
{
    [MetadataTypeAttribute(typeof(NewsItem.News_ItemMetadata))]
    public partial class NewsItem : DefaultConnectionDB.Record<NewsItem>  
    {
        public string ShortBody
        {
            get { return Body.Excerpt(500, 600); }
        }
        
        internal sealed class News_ItemMetadata
        {
            [AllowHtml,RemoveScript]
            public string Body { get; set; }
            [Required]
            public string Title { get; set; }
        }
    }
}