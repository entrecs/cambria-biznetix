﻿using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin
{
    public class BizAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BizAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BizAdmin_default",
                "BizAdmin/{controller}/{action}/{id}",
                new { controller = "BizAdmin", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}