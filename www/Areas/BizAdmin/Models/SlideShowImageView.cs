﻿using System;
using System.Collections.Generic;
using System.Linq;
using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;


namespace Biznetix.BizCMS.Data
{
    public class SlideShowImageViewModel 

    {
        public SlideShowImage slideShow { get; set; }
        public HttpPostedFileBase newImage { get; set; }

    }
}