﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;
using Biznetix.Common.StringExtensions;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    public class PagesController : Controller
    {
        // GET: Pages
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            ViewBag.GetDeparmentUsers = DepartmentUser.Fetch("").Where(x => x.UserId == User.Identity.GetUserId() && x.IsAssigned == true).ToList();
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> pages = cms_Page.Fetch("ORDER BY SORT, PAGENAME ASC");
            Session["tab_index"] = 0;
            return View(pages);
        }
        public ActionResult Create(int? parentpage)
        {

            ViewBag.IsAdmin = User.IsInRole("Admin");
            cms_Page newpage = new cms_Page() { Display = true };

            newpage.ParentPage = parentpage.HasValue ? parentpage.Value : 0;
            newpage.Sort = 0;
            newpage.PageTypeID = 1; //normal page

            ViewBag.PageHeaders = true;
            ViewBag.ENABLE_PAGE_ADD = true;
            ViewBag.ENABLE_SAVE_DRAFT = false;
            ViewBag.CAREERS_TOOL = false;
            ViewBag.NEWS_TOOL = false;
            ViewBag.PORTFOLIO_TOOL = false;
            ViewBag.TESTIMONIAL_TOOL = false;
            ViewBag.PCT_ADD_SECURE_PAGE = true;
            ViewBag.PCT_BIZNETIX_ONLY_OPTIONS_ON = User.IsInRole("SuperAdmin"); ;
            string biznetix_only_clause = "";

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> navpages = cms_Page.Fetch("");

            if (!ViewBag.PCT_BIZNETIX_ONLY_OPTIONS_ON)
            {
                biznetix_only_clause = "WHERE BiznetixOnly=0";
            }

            IEnumerable<Biznetix.BizCMS.Data.cms_PagesType> pagetypes = cms_PagesType.Fetch(biznetix_only_clause);
            //Nitesh-----
            var user = User.Identity.Name;

            var userId = AspNetUser.Fetch("").Where(x => x.UserName == User.Identity.Name).Select(y => y.Id).SingleOrDefault();
            List<Biznetix.BizCMS.Data.DepartmentUser> userDepartmentsList = DepartmentUser.Fetch("").Where(x => x.UserId == userId && x.IsAssigned == true).ToList();

            if (!User.IsInRole("Admin"))
            {
                bool isdepartmentExists = cms_Page.Fetch("").Any(x => x.PageName == "Departments");
                if (isdepartmentExists)
                {
                    var DepartmentTypeId = cms_Page.Fetch("").Where(x => x.PageName == "Departments").Select(y => y.PageID).FirstOrDefault();
                    ViewData["DepartmentTypeId"] = DepartmentTypeId;
                    
                    List<Biznetix.BizCMS.Data.cms_Page> navpages2 = new List<cms_Page>();
                    
                    foreach (cms_Page cp in navpages)
                    {
                        foreach (DepartmentUser du in userDepartmentsList)
                        {
                            if (du.UserId.Equals(cp.UserId) || du.DepartmentName.Equals(cp.PageName)) 
                            {
                                navpages2.Add(cp);
                                break;
                            }
                        }         
                    }
                    
                    ViewBag.AllPages = navpages2;
                    ViewBag.rootPageforDepartment = cms_Page.Fetch("").Where(x => x.PageName == "Departments").ToList();                
                }
            }
            else
            {
                ViewBag.AllPages = navpages;
            }

            
            //Nitesh--Allow only Dept allocated to user and created by the user
            ViewBag.pageTypes = pagetypes;

            ViewBag.version_count = 0;
            ViewBag.alt_version_count = 0;

            ViewBag.filecreated = false;
            // ViewBag.AllPages = navpages;
            ViewBag.msg = TempData["msg"];
            return View(newpage);
        }

        [HttpPost]
        public ActionResult Create(cms_Page page)
        {
            // clear the navigation cache
            HttpRuntime.Cache.Remove("Allpages");

            // TODO: Add insert logic here
            cms_Page newpage = new cms_Page() { Display = true }; ;

            if (page.PageName == null)
            {
                TempData["msg"] = "Page name is required!";
                return RedirectToAction("Create");
            }

            newpage.PageName = page.PageName;
            newpage.PageTypeID = page.PageTypeID;
            if (!User.IsInRole("Admin"))
            {
                //page.ParentPage = 2;
                newpage.ParentPage = page.ParentPage;
            }
            else
            {
                newpage.ParentPage = page.ParentPage;
            }
            newpage.SecurePage = page.SecurePage;
            newpage.Sort = page.Sort;
            newpage.SubPageFilename = page.SubPageFilename;
            newpage.SubPageType = page.SubPageType;
            newpage.BottomPageType = page.BottomPageType;
            newpage.BottomPageFilename = page.BottomPageFilename;
            newpage.Title = page.Title;
            newpage.CannotDelete = page.CannotDelete;
            newpage.CannotHaveChildren = page.CannotHaveChildren;
            newpage.canonical_url = page.canonical_url;
            newpage.CleanUrl = page.CleanUrl; 
            newpage.Description = page.Description;
            newpage.Display = page.Display;
            newpage.Editable = page.Editable;
            newpage.fileID = page.fileID;
            newpage.pHeader = page.pHeader;
            newpage.HeaderImage = page.HeaderImage;
            newpage.HeaderImageHeight = page.HeaderImageHeight;
            newpage.HeaderImageWidth = page.HeaderImageWidth;
            newpage.Keywords = page.Keywords;
            newpage.LastModified = DateTime.Now;
            newpage.Level = page.Level;
            newpage.LinkAddress = page.LinkAddress;
            newpage.LowerRight = page.LowerRight;
            newpage.og_admins = page.og_admins;
            newpage.og_country = page.og_country;
            newpage.og_description = page.og_description;
            newpage.og_image = page.og_image;
            newpage.og_latitude = page.og_latitude;
            newpage.og_locality = page.og_locality;
            newpage.og_longitude = page.og_longitude;
            newpage.og_region = page.og_region;
            newpage.og_siteName = page.og_siteName;
            newpage.og_streetAddress = page.og_streetAddress;
            newpage.og_title = page.og_title;
            newpage.og_type = page.og_type;
            newpage.og_url = page.og_url;
            newpage.UserId = User.Identity.GetUserId();
            var db = new PetaPoco.Database("DefaultConnection");
            var pageid = db.Insert("cms_Pages", "PageID", newpage);

            // Filename incorporates the PageID for uniqueness, so we need to update it right after the insert
            if (page.Filename != null)
            {
                newpage.Filename = page.Filename;
            }
            else
            { // generate a filename from title
                var sanitize = new Regex("[^A-Za-z0-9_-]");
                page.Filename = newpage.Filename = sanitize.Replace(page.PageName.Replace(" ", "-"), "") + "-" + pageid;
            }
            db.Update("cms_Pages", "PageID", newpage);

            string contentFile = Server.MapPath("/ContentPages/") + page.Filename + ".inc";

            System.IO.File.WriteAllText(contentFile, page.main_content);

            string altcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_alt.inc";
            string altcontent_Specified = Server.MapPath("/ContentPages/") + page.SubPageFilename + ".inc";
            string bottomcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_bottomcontent.inc";
            //string bottomcontent_specfied = Server.MapPath("/ContentPages/") + page.BottomPageFilename + ".inc";
            string bottomcontent_specfied = Server.MapPath(page.BottomPageFilename);

            string old_alt_text;

            if (page.SubPageType == "content" && (System.IO.File.Exists(altcontentFile)))
            {
                old_alt_text = System.IO.File.ReadAllText(altcontentFile);
                if (old_alt_text != page.alt_content)
                {
                    if (!System.IO.File.Exists(Server.MapPath("/ContentPages/history/" + page.Filename + "_alt")))
                    {
                        System.IO.File.Copy(altcontentFile, Server.MapPath("/ContentPages/history/" + page.Filename + "_alt"));
                        System.IO.File.WriteAllText(altcontentFile, page.alt_content);
                    }
                    else
                    {
                        System.IO.File.WriteAllText(Server.MapPath("/ContentPages/history/" + page.Filename + "_alt"), page.alt_content);
                        System.IO.File.WriteAllText(altcontentFile, page.alt_content);
                    }
                }
            }
            else if (page.SubPageType == "content")
            {
                System.IO.File.WriteAllText(altcontentFile, page.alt_content);
            }

            if (page.SubPageType == "file" && (!System.IO.File.Exists(altcontent_Specified)))
            {
                //System.IO.File.WriteAllText(altcontent_Specified, string.Empty);
            }

            if (page.BottomPageType == "content" && (System.IO.File.Exists(bottomcontentFile)))
            {
                old_alt_text = System.IO.File.ReadAllText(bottomcontentFile);
                if (old_alt_text != page.bottom_content)
                {
                    if (System.IO.File.Exists(Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent")))
                    {
                        System.IO.File.Delete(Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent"));
                    }
                    System.IO.File.Copy(bottomcontentFile, Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent"));
                    System.IO.File.WriteAllText(bottomcontentFile, page.bottom_content);
                }
            }
            else
            {
                System.IO.File.WriteAllText(bottomcontentFile, page.bottom_content);
            }

            if (page.BottomPageType == "file" && (!System.IO.File.Exists(bottomcontent_specfied)))
            {
                //System.IO.File.WriteAllText(bottomcontent_specfied, string.Empty);
            }

            if (page.ParentPage != 0) { 
                string parentpage = cms_Page.Fetch("").Where(x => x.PageID == page.ParentPage).FirstOrDefault().PageName;

                if (parentpage == "Departments")
                {
                    Department department = new Department();
                    department.DepartmentName = page.PageName;
                    department.DepartmentDescription = page.Description;
                    var departmentTypeId = department.Insert();

                    if (departmentTypeId != null && !User.IsInRole("Admin"))
                    {
                        DepartmentUser departmentUsr = new DepartmentUser();
                        departmentUsr.DepartmentName = page.PageName;
                        departmentUsr.DepartmentTypeId = Convert.ToInt32(departmentTypeId);
                        departmentUsr.UserId = User.Identity.GetUserId();
                        departmentUsr.IsAssigned = true;
                        departmentUsr.Save();
                    }
                }
            }

            TempData["msg"] = "New Page added!";
            return RedirectToAction("Edit", new { id = newpage.PageID });
        }

        public ActionResult Edit(int id)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            ViewBag.PageHeaders = true;
            ViewBag.ENABLE_PAGE_ADD = true;
            ViewBag.ENABLE_SAVE_DRAFT = false;
            ViewBag.CAREERS_TOOL = false;
            ViewBag.NEWS_TOOL = false;
            ViewBag.PORTFOLIO_TOOL = false;
            ViewBag.TESTIMONIAL_TOOL = false;
            ViewBag.PCT_ADD_SECURE_PAGE = true;
            ViewBag.PCT_BIZNETIX_ONLY_OPTIONS_ON = User.IsInRole("SuperAdmin"); ;
            string biznetix_only_clause = "";

            Biznetix.BizCMS.Data.cms_Page page = cms_Page.Single("WHERE PageID = " + id);

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> navpages = cms_Page.Fetch("ORDER BY sort, pagename");

            if (!ViewBag.PCT_BIZNETIX_ONLY_OPTIONS_ON)
            {
                biznetix_only_clause = "WHERE BiznetixOnly=0";
            }

            IEnumerable<Biznetix.BizCMS.Data.cms_PagesType> pagetypes = cms_PagesType.Fetch(biznetix_only_clause);

            ViewBag.pageTypes = pagetypes;

            string contentFile = Server.MapPath("/ContentPages/") + page.Filename + ".inc";
            string altcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_alt.inc";
            string bottomcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_bottomcontent.inc";


            int mainCount = 0, altCount = 0;
            if (System.IO.File.Exists(contentFile))
            {
                create_file_history(page.Filename, out mainCount, out altCount);
            }
            ViewBag.version_count = mainCount;
            ViewBag.alt_version_count = altCount;

            if (Session["tab_index"] != null && Session["tab_index"].ToString() != "")
                ViewBag.tab_index = Session["tab_index"].ToString();
            else
                ViewBag.tab_index = 0;

            if (System.IO.File.Exists(contentFile))
            {
                ViewBag.filecreated = true;
                ViewBag.content_txt = System.IO.File.ReadAllText(contentFile);
            }
            else
            {
                ViewBag.filecreated = false;
            }

            if (System.IO.File.Exists(altcontentFile))
            {
                ViewBag.altcontent_txt = System.IO.File.ReadAllText(altcontentFile);
            }
            else
            {
                ViewBag.altcontent_txt = "";
            }

            if (System.IO.File.Exists(bottomcontentFile))
            {
                ViewBag.bottomcontent_txt = System.IO.File.ReadAllText(bottomcontentFile);
            }
            else
            {
                ViewBag.bottomcontent_txt = "";
            }


            //string previousContent_path = string.Format("{0}{1}_{2}.inc", Server.MapPath("/ContentPages/history/"), page.Filename, (mainCount).ToString());
            //if (System.IO.File.Exists(previousContent_path))
            //{
            //    ViewBag.previousContent = System.IO.File.ReadAllText(previousContent_path);
            //}
            //else
            //{
            //    ViewBag.previousContent = string.Empty;
            //}
            //ViewBag.AllPages = navpages;

            //return View(page);
            string previousContent_path = string.Format("{0}{1}_{2}.inc", Server.MapPath("/ContentPages/history/"), page.Filename, (mainCount).ToString());
            if (System.IO.File.Exists(previousContent_path))
            {
                ViewBag.previousContent = System.IO.File.ReadAllText(previousContent_path);
            }
            else
            {
                ViewBag.previousContent = string.Empty;
            }
            //get the assigned departments based on user id and compare the page names and get the records. "getPageTreeAndDisplayAsOptions" check this function to add
            if (!User.IsInRole("Admin"))
            {
                //int? DepartmentPageId = navpages.Where(x => x.PageName == "Departments" && x.ParentPage == 0).FirstOrDefault().ParentPage;
                // ViewBag.AllPages = navpages.Where(x => x.ParentPage == DepartmentPageId || (x.PageName == "Deparments" && x.ParentPage == 0)).ToList();
                
                //Nitesh- Allow only Dept allocated to user and created by the user
                var user = User.Identity.Name;
                bool isdepartmentExists = cms_Page.Fetch("").Any(x => x.PageName == "Departments");
                
                if (isdepartmentExists)
                {
                    var DepartmentTypeId = cms_Page.Fetch("").Where(x => x.PageName == "Departments").Select(y => y.PageID).FirstOrDefault();
                    ViewData["DepartmentTypeId"] = DepartmentTypeId;

                    var userId = AspNetUser.Fetch("").Where(x => x.UserName == User.Identity.Name).Select(y => y.Id).SingleOrDefault();
                    List<Biznetix.BizCMS.Data.DepartmentUser> userDepartmentsList = DepartmentUser.Fetch("").Where(x => x.UserId == userId && x.IsAssigned == true).ToList();

                    List<Biznetix.BizCMS.Data.cms_Page> navpages2 = new List<cms_Page>();

                    foreach (cms_Page cp in navpages)
                    {
                        foreach (DepartmentUser du in userDepartmentsList)
                        {
                            if (du.UserId.Equals(cp.UserId) || du.DepartmentName.Equals(cp.PageName))
                            {
                                navpages2.Add(cp);
                                break;
                            }
                        }
                    }
                    
                    ViewBag.AllPages = navpages2;
                    ViewBag.rootPageforDepartment = cms_Page.Fetch("").Where(x => x.PageName == "Departments").ToList();
                }
            }
            else
            {
                ViewBag.AllPages = navpages;
            }
            return View(page);
        }

        [HttpPost]
        public ActionResult Edit(int id, cms_Page page, HttpPostedFileBase file)
        {
            HttpRuntime.Cache.Remove("Allpages");
            try
            {
                cms_Page update = cms_Page.Single("WHERE PageID=" + id);

                update.PageName = page.PageName;
                update.PageTypeID = page.PageTypeID;

                if (!User.IsInRole("Admin"))
                {
                    page.ParentPage = page.ParentPage;
                    update.ParentPage = page.ParentPage;
                }
                else
                {
                    if (page.ParentPage != null)
                    {
                        update.ParentPage = page.ParentPage;
                    }
                }
                
                update.SecurePage = page.SecurePage;
                update.Sort = page.Sort;
                update.SubPageFilename = page.SubPageFilename;
                update.SubPageType = page.SubPageType;
                update.BottomPageFilename = page.BottomPageFilename;
                update.BottomPageType = page.BottomPageType;
                update.Title = page.Title;
                update.CannotDelete = page.CannotDelete;
                update.CannotHaveChildren = page.CannotHaveChildren;
                update.canonical_url = page.canonical_url;
                update.CleanUrl = page.CleanUrl;
                update.Description = page.Description;
                update.Display = page.Display;
                update.Editable = page.Editable;
                update.fileID = page.fileID;
                update.Filename = page.Filename;
                update.pHeader = page.pHeader;
                update.HeaderImage = page.HeaderImage;
                update.HeaderImageHeight = page.HeaderImageHeight;
                update.HeaderImageWidth = page.HeaderImageWidth;
                update.Keywords = page.Keywords;
                update.LastModified = DateTime.Now;
                update.Level = page.Level;
                update.LinkAddress = page.LinkAddress;
                update.LowerRight = page.LowerRight;
                update.og_admins = page.og_admins;
                update.og_country = page.og_country;
                update.og_description = page.og_description;
                if (file != null)
                {
                    var uploadedFile = Request.Files["file"];
                    string imageName = uploadedFile.FileName.Split('.')[0].ToString() + "_" + id + System.IO.Path.GetExtension(uploadedFile.FileName);
                    string savePath = Server.MapPath(Biznetix.BizCMS.Helpers.Constants.EditPage_TabSocialImage.SaveImagePath.Description() + imageName);
                    string file_ext = System.IO.Path.GetExtension(uploadedFile.FileName).ToUpper();

                    if (file_ext == ".JPG" || file_ext == ".PNG" || file_ext == ".GIF")
                    {
                        uploadedFile.SaveAs(savePath);
                    }
                    page.og_image = imageName;
                    update.og_image = page.og_image;
                }
                update.og_latitude = page.og_latitude;
                update.og_locality = page.og_locality;
                update.og_longitude = page.og_longitude;
                update.og_region = page.og_region;
                update.og_siteName = page.og_siteName;
                update.og_streetAddress = page.og_streetAddress;
                update.og_title = page.og_title;
                update.og_type = page.og_type;
                update.og_url = page.og_url;

                update.Update();

                string contentFile = Server.MapPath("/ContentPages/") + page.Filename + ".inc";
                string altcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_alt.inc";
                string altcontent_Specified = Server.MapPath("/ContentPages/") + page.SubPageFilename + ".inc";
                string bottomcontentFile = Server.MapPath("/ContentPages/") + page.Filename + "_bottomcontent.inc";
                //string bottomcontent_specfied = Server.MapPath("/ContentPages/") + page.BottomPageFilename + ".inc";
                string bottomcontent_specfied = Server.MapPath(page.BottomPageFilename);
                
                string old_content_text, old_alt_text;
                int mainCount = 0, altCount = 0;
                //if (System.IO.File.Exists(contentFile))
                //{
                create_file_history(page.Filename, out mainCount, out altCount);
                //}

                if (System.IO.File.Exists(contentFile))
                {
                    old_content_text = System.IO.File.ReadAllText(contentFile);
                    Response.Write(page.main_content);
                    if (old_content_text != page.main_content)
                    {
                        //if (System.IO.File.Exists(Server.MapPath("/ContentPages/history/" + page.Filename + "." + (mainCount + 1).ToString())))
                        //{
                        //    System.IO.File.Delete(Server.MapPath("/ContentPages/history/" + page.Filename + "." + (mainCount + 1).ToString()));
                        //}
                        string dest_filename = string.Format("{0}{1}_{2}.inc", Server.MapPath("/ContentPages/history/"), page.Filename, (mainCount + 1).ToString());
                        if (!System.IO.File.Exists(dest_filename))
                        {
                            System.IO.File.Copy(contentFile, dest_filename);
                            System.IO.File.WriteAllText(contentFile, page.main_content.SanitizeHTML());
                        }
                        else
                        {
                            System.IO.File.WriteAllText(dest_filename, page.main_content.SanitizeHTML());
                            System.IO.File.WriteAllText(contentFile, page.main_content.SanitizeHTML());
                        }
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(contentFile, page.main_content.SanitizeHTML());
                }

                if (page.SubPageType == "content" && (System.IO.File.Exists(altcontentFile)))
                {
                    old_alt_text = System.IO.File.ReadAllText(altcontentFile);
                    if (old_alt_text != page.alt_content)
                    {
                        if (!System.IO.File.Exists(Server.MapPath("/ContentPages/history/" + page.Filename + "_alt")))
                        {
                            System.IO.File.Copy(altcontentFile, Server.MapPath("/ContentPages/history/" + page.Filename + "_alt"));
                            System.IO.File.WriteAllText(altcontentFile, page.alt_content.SanitizeHTML());
                        }
                        else
                        {
                            System.IO.File.WriteAllText(Server.MapPath("/ContentPages/history/" + page.Filename + "_alt"), page.alt_content);
                            System.IO.File.WriteAllText(altcontentFile, page.alt_content.SanitizeHTML());
                        }
                    }
                }
                else if (page.SubPageType == "content")
                {
                    System.IO.File.WriteAllText(altcontentFile, page.alt_content.SanitizeHTML());
                }

                if (page.SubPageType == "file" && (!System.IO.File.Exists(altcontent_Specified)))
                {
                    //System.IO.File.WriteAllText(altcontent_Specified, string.Empty);
                }

                if (page.BottomPageType == "content" && (System.IO.File.Exists(bottomcontentFile)))
                {
                    old_alt_text = System.IO.File.ReadAllText(bottomcontentFile);
                    if (old_alt_text != page.bottom_content)
                    {
                        if (System.IO.File.Exists(Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent")))
                        {
                            System.IO.File.Delete(Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent"));
                        }
                        System.IO.File.Copy(bottomcontentFile, Server.MapPath("/ContentPages/history/" + page.Filename + "_bottomcontent"));
                        System.IO.File.WriteAllText(bottomcontentFile, page.bottom_content.SanitizeHTML());
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(bottomcontentFile, page.bottom_content.SanitizeHTML());
                }

                if (page.BottomPageType == "file" && (!System.IO.File.Exists(bottomcontent_specfied)))
                {
                    //System.IO.File.WriteAllText(bottomcontent_specfied, string.Empty);
                }

                HttpRuntime.Cache.Remove("Allpages");
                
                IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = cms_Page.Fetch("ORDER BY Sort, PageName"); 
                if (allpages != null)
                {
                    HttpRuntime.Cache["Allpages"] = allpages;
                }


                ViewBag.version_count = mainCount;
                ViewBag.alt_version_count = altCount;
                Session["tab_index"] = 0;

                if (Request.Form["sideType"] != null &&
                    Request.Form["sideType"].Equals("1"))
                {
                    Session["tab_index"] = 1;
                    return RedirectToAction("Edit", new { id = id });
                } else if (Request.Form["bottomType"] != null &&
                    Request.Form["bottomType"].Equals("1"))
                {
                    Session["tab_index"] = 5;
                    return RedirectToAction("Edit", new { id = id });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                TempData["msg"] = "There was an error saving your changes.";
                return RedirectToAction("Edit", new { id = id });

            }
        }

        public ActionResult Delete(int id)
        {
            cms_Page page = cms_Page.Single("WHERE PageID=" + id);

            var db = new PetaPoco.Database("DefaultConnection");
            db.Delete("cms_Pages", "PageID", null, id);
            // clear the navigation cache
            HttpRuntime.Cache.Remove("Allpages");
            //delete departments
            if (page.PageName == "Departments" && page.ParentPage == 0)
            {
                Department department = Department.Fetch("").Where(x => x.DepartmentName == page.PageName).FirstOrDefault();
                if (department != null)
                {
                    DepartmentUser departmentUsr = DepartmentUser.Fetch("").Where(x => x.DepartmentTypeId == department.DepartmentTypeId).FirstOrDefault();
                    if (departmentUsr != null)
                    {
                        departmentUsr.Delete();
                    }
                    Department.Delete(department.DepartmentTypeId);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void create_file_history(string filename, out int mainCount, out int altCount)
        {

            string[] files = System.IO.Directory.GetFiles(Server.MapPath("/ContentPages/history/"), filename + "_*");
            string[] altfiles = System.IO.Directory.GetFiles(Server.MapPath("/ContentPages/history/"), filename + "_alt.*");
            mainCount = files.Length;
            altCount = altfiles.Length;
        }
        public ActionResult ReadContent(string filename)
        {
            string filePath = Server.MapPath("/ContentPages/history/") + filename + ".inc";
            string fileContent = System.IO.File.ReadAllText(filePath);
            ViewBag.FileContent = fileContent;
            return Json(new { fileContent }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveHistoryContent(string filename, string versionCount, string pageID)
        {
            string contentFile = Server.MapPath("/ContentPages/history/") + filename + ".inc";
            if (System.IO.File.Exists(contentFile))
            {
                string fileContent = System.IO.File.ReadAllText(contentFile);
                string current_filename = cms_Page.Single("WHERE PageID = " + pageID).Filename;
                string filePath = Server.MapPath("/ContentPages/") + current_filename + ".inc";
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.WriteAllText(filePath, fileContent);
                    return RedirectToAction("Edit", "BizAdmin/Pages", new { id = pageID });
                }
            }
            return RedirectToAction("Edit", "BizAdmin/Pages", new { id = pageID });
        }
        [AllowAnonymous]
        public ActionResult Uploadfile(HttpPostedFileBase fileData, string categoryId, bool isCancel = false)
        {
            if (fileData != null)
            {
            }
            return Content(fileData.FileName);
        }
        [HttpPost]
        [ValidateInput(false)]
        //[Authorize(Roles = "Admin")]
        public ActionResult UpdateContent(string id, string content)
        {
            string contentFile = Server.MapPath("/ContentPages/") + id.Replace("bizcontent_", "") + ".inc";

            if (System.IO.File.Exists(contentFile))
            {
                System.IO.File.WriteAllText(contentFile, content.SanitizeHTML());
                return Content("true");
            }
            else
            {
                Response.StatusCode = 500;
                return Content(" Content file not found.");
            }
        }
    }
}