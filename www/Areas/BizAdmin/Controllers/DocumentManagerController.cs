﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Helpers;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DocumentManagerController : Controller
    {
        // GET: BizAdmin/DocumentManager
        public ActionResult Index(string ContentPage = "FolderManager",string Message = "")
        {
            ViewBag.Message = Message;
            ViewBag.ContentPage = ContentPage;
            return View();
        }
    }
}