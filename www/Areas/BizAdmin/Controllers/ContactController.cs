﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    public class ContactController : Controller
    {
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableContact'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View();
        }
    }
}