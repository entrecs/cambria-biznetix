﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FileManagerController : Controller
    {
        [HttpGet]
        public ActionResult Index(string FileName)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                FST_File files = FST_File.Fetch("").Where(x => x.FileName == FileName).FirstOrDefault();
                if (files != null)
                {
                    if (System.IO.File.Exists(Server.MapPath("/ContentPages/Documents/SharedDocs/") + FileName + "." + files.FileName.Split('.')[1]))
                    {
                        ViewBag.DocFileName = Server.MapPath("/ContentPages/Documents/SharedDocs/") + FileName + "." + files.FileName.Split('.')[1];
                        string contentType = GetMimeType(files.FileName.Split('.')[1].ToString());
                        return File(ViewBag.DocFileName, contentType);
                    }
                    else {
                        ViewBag.Message = "file not found";
                    }
                }
            }

            return View();
        }

        #region CRUD Methods
        public ActionResult Create()
        {
            ViewBag.Action = "Create";
            IEnumerable<SelectListItem> folderList = (from c in FST_Folder.Fetch("").Where(x => x.Active == true)
                                                      select new SelectListItem
                                                     {
                                                         Text = c.FolderName,
                                                         Value = c.FolderID.ToString()
                                                     });
            ViewBag.FolderList = folderList;
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "FileID")]FST_File docFile, IEnumerable<HttpPostedFileBase> file)
        {
            if (ModelState.IsValid)
            {
                FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", docFile.FolderID).FirstOrDefault();
                int nxtFileID = FST_File.Fetch("").Count > 0 ? FST_File.Fetch("").Max(x => x.FileID) + 1 : 1;
                string path = Server.MapPath(string.Format("{0}{1}_{2}\\", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName, Selectedfolder.FolderID));

                if (Directory.Exists(path))
                {
                    var uploadedFile = Request.Files["file"];
                    string filename = string.Format("{0}_{1}.{2}", uploadedFile.FileName.Split('.')[0], nxtFileID, uploadedFile.FileName.Split('.')[1]);
                    string filepath = string.Format("{0}{1}", path, filename);
                    docFile.FileName = filename;
                    uploadedFile.SaveAs(filepath);
                    if (System.IO.File.Exists(filepath))
                    {
                        System.IO.File.Delete(Server.MapPath(Constants.DocumentPathSettings.SharedDirectoryPath.Description() + docFile.FileName + "." + uploadedFile.FileName.Split('.')[1]));
                        System.IO.File.Copy(filepath, Server.MapPath(Constants.DocumentPathSettings.SharedDirectoryPath.Description() + docFile.FileName + "." + uploadedFile.FileName.Split('.')[1]));
                    }
                    else {
                        System.IO.File.Copy(filepath, Server.MapPath(Constants.DocumentPathSettings.SharedDirectoryPath.Description() + docFile.FileName + "." + uploadedFile.FileName.Split('.')[1]));
                    }
                    docFile.FileSize = uploadedFile.ContentLength;
                }
                else
                {
                    string noficationmsg = "The file is not saved. may be the folder is moved or not found";
                    return RedirectToAction("Index", "DocumentManager", new { ContentPage = "FileManager", Message = noficationmsg });
                }
                docFile.DateTime = DateTime.Now.Date;
                //docFile.Active = true;
                var inserted = DefaultConnectionDB.GetInstance().Insert("FST_Files", "FileID", docFile);
                return RedirectToAction("Index", "DocumentManager", new { ContentPage = "FileManager" });
            }
            else
            {
                ModelState.AddModelError("required", "Folder, Title, select File are requied fields");
                return RedirectToAction("Create");
            }
        }
        public ActionResult Edit(int id)
        {
            ViewBag.Action = "Edit";
            IEnumerable<SelectListItem> folderList = (from c in FST_Folder.Fetch("")
                                                      select new SelectListItem
                                                      {
                                                          Text = c.FolderName,
                                                          Value = c.FolderID.ToString()
                                                      });
            ViewBag.FolderList = folderList;
            FST_File editfile = FST_File.Single(id);
            return View(editfile);
        }
        [HttpPost]
        public ActionResult Edit(FST_File editFile)
        {
            if (ModelState.IsValid)
            {
                FST_File file = FST_File.Single(editFile.FileID);
                editFile.FileName = file.FileName;
                editFile.FileSize = file.FileSize;
                editFile.DateTime = file.DateTime;

                if (editFile.FolderID != file.FolderID)
                {
                    FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", editFile.FolderID).FirstOrDefault();
                    FST_Folder Prevfolder = FST_Folder.Fetch("Where FolderID=@0", file.FolderID).FirstOrDefault();
                    string sourcePath = Server.MapPath(string.Format("{0}{1}_{2}\\", Constants.DocumentPathSettings.DirectoryPath.Description(), Prevfolder.FolderName, Prevfolder.FolderID));
                    string destPath = Server.MapPath(string.Format("{0}{1}_{2}\\", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName, Selectedfolder.FolderID));
                    if (Directory.Exists(sourcePath))
                    {
                        string srcfilepath = string.Format("{0}{1}", sourcePath, editFile.FileName);
                        string destfilepath = string.Format("{0}{1}", destPath, editFile.FileName);
                        System.IO.File.Move(srcfilepath, destfilepath);
                    }
                }
                if (editFile.FileName != file.FileName)
                {
                    FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", editFile.FolderID).FirstOrDefault();
                    FST_Folder Prevfolder = FST_Folder.Fetch("Where FolderID=@0", file.FolderID).FirstOrDefault();
                    string sourcePath = Server.MapPath(string.Format("{0}{1}", Constants.DocumentPathSettings.SharedDirectoryPath.Description(), file.FileName + "." + file.FileName.Split('.')[1]));
                    if (System.IO.File.Exists(sourcePath))
                    {
                        System.IO.File.Move(sourcePath, Server.MapPath(Constants.DocumentPathSettings.SharedDirectoryPath.Description() + editFile.FileName + "." + file.FileName.Split('.')[1]));
                    }
                }

                editFile.Update();
            }
            return RedirectToAction("Index", "DocumentManager", new { ContentPage = "FileManager" });
        }
        [AllowAnonymous]
        public ActionResult Uploadfile(HttpPostedFileBase fileData, string folderID)
        {
            if (fileData != null)
            {
                FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", folderID).FirstOrDefault();
                int nxtFileID = FST_File.Fetch("").Count > 0 ? FST_File.Fetch("").Max(x => x.FileID) + 1 : 1;
                string path = Server.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName, Selectedfolder.FolderID));
                if (Directory.Exists(path))
                {
                    var uploadedFile = Request.Files["filedata"];
                    string ads = path + "_" + uploadedFile.FileName + "_" + nxtFileID;
                    //uploadedFile.SaveAs(path);
                }
            }
            return Json(new { FileName = "" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadFile(int Id)
        {
            FST_File files = FST_File.Fetch("Where FileID=@0", Id).FirstOrDefault();
            return Json(new { file = files }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IsFileExist(string fileName)
        {
            FST_File files = FST_File.Fetch("").Where(x => x.FileName == fileName).FirstOrDefault();
            if (files != null)
            {
                if (System.IO.File.Exists(Server.MapPath("/ContentPages/Documents/SharedDocs/") + fileName + "." + files.FileName.Split('.')[1]))
                {
                    string url = Server.MapPath("/ContentPages/Documents/SharedDocs/") + fileName + "." + files.FileName.Split('.')[1];
                    return Json(new { IsExist = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { IsExist = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadFile(int Id)
        {
            FST_File files = FST_File.Fetch("Where FileID=@0", Id).FirstOrDefault();
            string folderName = FST_Folder.Fetch("Where FolderId=@0", files.FolderID).Select(x => x.FolderName).FirstOrDefault();
            if (files != null)
            {
                string sourcePath = Server.MapPath(string.Format("{0}{1}_{2}\\{3}", Constants.DocumentPathSettings.DirectoryPath.Description(), folderName, files.FolderID, files.FileName));
                string contentType = GetMimeType(files.FileName.Split('.')[1].ToString());
                return File(sourcePath, contentType, files.Title);
            }
            return Json(new { file = "" }, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public string GetMimeType(string fileExt)
        {
            string contentType = string.Empty;
            switch (fileExt)
            {
                case "pdf":
                    contentType = Constants.MIME.PDF.Description();
                    break;
                case "txt":
                    contentType = Constants.MIME.TXT.Description();
                    break;
                case "doc":
                    contentType = Constants.MIME.DOC.Description();
                    break;
                case "docx":
                    contentType = Constants.MIME.DOCX.Description();
                    break;
                case "xls":
                    contentType = Constants.MIME.XLS.Description();
                    break;
                case "jpg":
                    contentType = Constants.MIME.JPG.Description();
                    break;
                case "png":
                    contentType = Constants.MIME.PNG.Description();
                    break;
                default:
                    break;
            }
            return contentType;
        }
        #endregion
    }
}