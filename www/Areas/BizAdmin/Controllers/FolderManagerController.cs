﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Helpers;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FolderManagerController : Controller
    {
        // GET: BizAdmin/DocumentManager
        public ActionResult Index(string Contentpage = "DocumentManager")
        {
            return View();
        }

        #region CRUD Methods
        public ActionResult Create()
        {        
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "FolderID")]FST_Folder folder)
        {
            if (!string.IsNullOrEmpty(folder.FolderName))
            {
                folder.DateAdded = DateTime.Now;
                var inserted = DefaultConnectionDB.GetInstance().Insert("FST_Folders", "FolderID", folder);
                if (Convert.ToInt32(inserted) > 0)
                {
                    string path = Server.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), folder.FolderName, Convert.ToInt32(inserted).ToString()));
                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }
                }
                return RedirectToAction("Index", "DocumentManager");
            }
            else
            {
                ViewBag.Message = "Folder name is a required field.";
                return View();
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {        
            FST_Folder editfolder = FST_Folder.Single(id);
            return View(editfolder);
        }

        [HttpPost]
        public ActionResult Edit(FST_Folder editfolder)
        {
            if (ModelState.IsValid)
            {
                FST_Folder folder = FST_Folder.Single(editfolder.FolderID);
                string sourcePath = Server.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), folder.FolderName, folder.FolderID));
                string destPath = Server.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), editfolder.FolderName, editfolder.FolderID));
                if (!folder.FolderName.Equals(editfolder.FolderName))
                {
                    if (Directory.Exists(sourcePath))
                    {
                        Directory.Move(sourcePath, destPath);
                    }
                }
                editfolder.Update();
                return RedirectToAction("Index", "DocumentManager");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetFiles(int Id)
        {
            FST_Folder folder = FST_Folder.Single(Id);
            List<FST_File> files = FST_File.Fetch("Where FolderID=@0", Id);
            return Json(new { file = files, folder = folder }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}