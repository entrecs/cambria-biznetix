﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
   [Authorize(Roles = "Admin")]
    public class DrivingDirectionController : Controller
    {
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableLocations'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View();
        }
        #region CRUD Methods
        public ActionResult Create()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableLocations'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "LocationID")]CompanyLocation location)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableLocations'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                if (!location.Active)
                {
                    location.Save();
                    return RedirectToAction("Index");
                }
                else
                {
                    bool checkActiveLoc = CompanyLocation.Fetch("Order By Name").Any(x => x.Active);
                    if (!checkActiveLoc)
                    {
                        location.Save();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.errorMessage = "Active Location should be one.. please remove the existing and add the new one";
                        return View();
                    }
                }
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableLocations'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            CompanyLocation events = CompanyLocation.Single(id);
            return View(events);
        }

        [HttpPost]
        public ActionResult Edit(CompanyLocation location)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableLocations'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                location.Save();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        #endregion
    }
}