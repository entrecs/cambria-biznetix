﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS.Helpers;
using System.Web.Security;
using System.Security.Principal;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
   
    public class GalleryController : Controller
    {
        // GET: BizAdmin/Gallery
   [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            List<MediaGalleryCategory> galleries = DefaultConnectionDB.GetInstance().Fetch<MediaGalleryCategory>(PetaPoco.Sql.Builder
                .Append("SELECT MediaGalleryCategories.*, ImagePath as PrimaryImageUrl")
                .Append("FROM MediaGalleryCategories LEFT JOIN MediaGalleryImages ON MediaGalleryCategories.FrontImageId=MediaGalleryImages.ImageId ORDER BY CategoryId"));

            List<int> GalleryPhotoCount = DefaultConnectionDB.GetInstance().Query<int>("SELECT count(dbo.[MediaGalleryCategories].CategoryID) As PhotoCount FROM dbo.[MediaGalleryCategories] left join dbo.[MediaGalleryImages] on MediaGalleryCategories.CategoryID=MediaGalleryImages.CategoryId GROUP BY MediaGalleryCategories.[CategoryId] ORDER BY MediaGalleryCategories.CategoryId").ToList();

            ViewBag.Galleries = galleries;
            ViewBag.GalleryPhotoCount = GalleryPhotoCount;

            ViewBag.nextCategoryId = DefaultConnectionDB.GetInstance().Query<int?>("select max([CategoryID]) + 1 from [dbo].[MediaGalleryCategories]").FirstOrDefault();
            ViewBag.nextImageId = DefaultConnectionDB.GetInstance().Query<int?>("select max([ImageId]) from [dbo].[MediaGalleryImages]").FirstOrDefault();
            
            return View();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult ShowGallery(string categoryName)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (string.IsNullOrEmpty(categoryName))
            {
                //throw new HttpException(404, "Gallery not found");
                return new HttpNotFoundResult();
            }
            //var galleryname = categoryName.Replace("-", " ");
            var gallery = MediaGalleryCategory.SingleOrDefault("WHERE CategoryName like @0", categoryName);
            if (gallery != null)
            {
                ViewBag.GalleryCategoryId = gallery.CategoryID;
                ViewBag.GalleryCategoryName = gallery.CategoryName;
                ViewBag.FrontImageId = gallery.FrontImageId;
                try
                {
                    ViewBag.nextImageId = DefaultConnectionDB.GetInstance().Query<int>("select max([ImageId]) from [dbo].[MediaGalleryImages]").FirstOrDefault();
                }
                catch
                {
                    ViewBag.nextImageId = 1;
                }
                    List<MediaGalleryImage> images = MediaGalleryImage.Fetch("WHERE CategoryId=@0 ORDER BY SORT", gallery.CategoryID);
                
                return View(images);
            }
            else
            {
                List<MediaGalleryCategory> galleries = MediaGalleryCategory.Fetch("");
                return View("ManageGallery", galleries);
            }
        }
        #region Gallery
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Index(List<MediaGalleryImage> uploadedImages, string FbAlbumId)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (uploadedImages != null && uploadedImages.Count > 0)
            {
                string[] Title = uploadedImages[0].Title.Split('_');
                string cleanUrl = DateTime.Now.Year + "-" + Title[1].Replace(" ", "-");
                bool isCleanUrlExists = DefaultConnectionDB.GetInstance().Query<MediaGalleryCategory>("select * from [dbo].[MediaGalleryCategories] where cleanUrl ='" + cleanUrl + "'").Any();
                if (isCleanUrlExists)
                {
                    return Json(new { HasError = false, Message = "Title name should be unique." });
                }

                MediaGalleryCategory category = new MediaGalleryCategory();
                category.CategoryID = Convert.ToInt32(uploadedImages[0].CategoryId);
                category.Title = Title[1].ToString();
                category.CategoryName = Title[1].ToString();
                category.FacebookAlbumId = string.IsNullOrEmpty(FbAlbumId) ? null : FbAlbumId == "0" ? null : FbAlbumId;
                category.Description = Title[2].ToString();
                category.FrontImageId = Convert.ToInt32(uploadedImages[0].ImageId);
                category.LastModified = DateTime.Now;
                category.CannotDelete = false;
                category.CleanUrl = DateTime.Now.Year + "-" + Title[1].Replace(" ", "-");
                DefaultConnectionDB.GetInstance().Insert("MediaGalleryCategories", "CategoryID", category);

                for (int i = 0; i < uploadedImages.Count; i++)
                {
                    if (uploadedImages[i].ImageId > 0 && category.CategoryID > 0)
                    {
                        MediaGalleryImage galleryImage = new MediaGalleryImage();
                        galleryImage.ImageId = uploadedImages[i].ImageId;
                        galleryImage.ImagePath = uploadedImages[i].ImagePath.Split('/').Last();
                        galleryImage.CategoryId = category.CategoryID;
                        galleryImage.Title = uploadedImages[i].Title.Split('_')[0];
                        var inserted_id = DefaultConnectionDB.GetInstance().Insert("MediaGalleryImages", "ImageId", galleryImage);
                        category.FrontImageId = Convert.ToInt32(inserted_id);
                        category.Update();
                    }
                }

            }
            return Json(new { HasError = false, Message = string.Empty }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteGallery(int galleryCategoryId)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (galleryCategoryId != 0)
            {
                DefaultConnectionDB.GetInstance().Execute("DELETE FROM MediaGalleryImages WHERE CategoryId=" + galleryCategoryId);
                DefaultConnectionDB.GetInstance().Execute("DELETE FROM MediaGalleryCategories WHERE CategoryID=" + galleryCategoryId);
                return Json(new { HasError = false }, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(new { HasError = true }); }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult EditGallery(int Id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (string.IsNullOrEmpty(Id.ToString()) && Id == 0)
            {
                return Json(new { HasError = true });
                throw new HttpException(404, "Gallery not found");
                //return new HttpNotFoundResult();             
            }
            else
            {
                var gallery = MediaGalleryCategory.SingleOrDefault("WHERE CategoryID like @0", Id);
                return Json(new { HasError = false, Gallery = gallery }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateGallery(MediaGalleryCategory galleryCategory)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            var gallery = MediaGalleryCategory.SingleOrDefault("WHERE CategoryID like @0", galleryCategory.CategoryID);
            gallery.Title = galleryCategory.Title;
            gallery.Description = galleryCategory.Description;
            gallery.Update();
            return Json(new { HasError = false, CategoryName = gallery.CategoryName }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Gallery Image
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult SaveGalleryImages(List<MediaGalleryImage> uploadedImages, string CategoryName)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (uploadedImages != null && uploadedImages.Count > 0)
            {                
                for (int i = 0; i < uploadedImages.Count; i++)
                {
                    MediaGalleryImage galleryImage = new MediaGalleryImage();
                    galleryImage.ImageId = uploadedImages[i].ImageId;
                    galleryImage.ImagePath = uploadedImages[i].ImagePath.Split('/').Last();
                    galleryImage.CategoryId = uploadedImages[i].CategoryId;
                    galleryImage.Title = uploadedImages[i].Title;
                    var inserted_id = DefaultConnectionDB.GetInstance().Insert("MediaGalleryImages", "ImageId", galleryImage);                
                }
            }
            return Json(new { HasError = false, Message = string.Empty, categoryName = CategoryName }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateGalleryImage(MediaGalleryImage galleryCategoryImage, string categoryName, string FrontImageId)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            var galleryImage = MediaGalleryImage.SingleOrDefault("WHERE ImageId like @0", galleryCategoryImage.ImageId);
            galleryImage.Title = galleryCategoryImage.Title;
            galleryImage.Update();
            if (!string.IsNullOrEmpty(FrontImageId))
            {
                MediaGalleryCategory galleryCategory = MediaGalleryCategory.SingleOrDefault("WHERE CategoryID like @0", galleryCategoryImage.CategoryId);
                galleryCategory.FrontImageId = Convert.ToInt32(FrontImageId);
                galleryCategory.Update();
            }
            return Json(new { HasError = false, CategoryName = categoryName }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteGalleryImage(int imgid, string categoryName)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (imgid != 0)
            {
                var galleryImage = MediaGalleryCategory.SingleOrDefault("WHERE FrontImageId like @0", imgid);
                if (galleryImage == null)
                {
                    DefaultConnectionDB.GetInstance().Execute("DELETE FROM MediaGalleryImages WHERE ImageId=" + imgid);
                    return Json(new { HasError = false, categoryName = categoryName }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { HasError = true, Message = "The image is referred as gallery cover photo, pls remove that and try again!..", categoryName = categoryName }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            { return Json(new { HasError = true }); }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult CancelUploadImage(List<MediaGalleryImage> uploadedImages, string categoryName)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (uploadedImages != null && uploadedImages.Count > 0)
            {
                for (int i = 0; i < uploadedImages.Count; i++)
                {
                    var Path = Server.MapPath(@"~\Content\Gallery\" + uploadedImages[i].ImagePath.Split('/').Last());
                    if (System.IO.File.Exists(Path))
                    {
                        System.IO.File.Delete(Path);
                    }
                }
            }
            System.Web.HttpContext.Current.Session["nxtImageId"] = null;
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return Json(new { HasError = false, categoryName = categoryName });
        }
        [Authorize(Roles = "Admin")]
        public JsonResult UpdateSort(int CategoryID, List<int> SortedList)
        {

            int sort = 0;
            var db = DefaultConnectionDB.GetInstance();
            
            foreach(int imageId in SortedList)
            {
                db.Execute("UPDATE MediaGalleryImages Set Sort=@0 where ImageId=@1 and CategoryId=@2", sort++, imageId, CategoryID);
            }
            
            return Json (new { HasError = false, Message = "Done" });
        }
        #endregion

        #region Helper Methods
        [AllowAnonymous]
        public ActionResult Uploadfile(HttpPostedFileBase fileData, string categoryId, bool isCancel=false)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableGallery'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            int nextImageID = 1;
            if (fileData != null)
            {
                if (isCancel)
                {
                    System.Web.HttpContext.Current.Session["nxtImageId"] = null;
                }
                if (System.Web.HttpContext.Current.Session["nxtImageId"] == null)
                {
                    try
                    {
                        ViewBag.nextCategoryId = DefaultConnectionDB.GetInstance().Query<int>("select max([CategoryID]) + 1 from [dbo].[MediaGalleryCategories]").FirstOrDefault();
                    }
                    catch
                    {
                        ViewBag.nextCategoryId = 1;
                    }
                    try
                    {
                        nextImageID = DefaultConnectionDB.GetInstance().Query<int>("select max([ImageId]) from [dbo].[MediaGalleryImages]").FirstOrDefault() + 1;
                    }
                    catch
                    {
                     
                    }
                    System.Web.HttpContext.Current.Session["nxtImageId"] = ViewBag.nextImageId;
                }
                

                var uploadedFile = Request.Files["filedata"];
                string imageName = uploadedFile.FileName.Split('.')[0].ToString() + System.IO.Path.GetExtension(uploadedFile.FileName);
                string saveName = imageName.Split('.')[0] + "_" + nextImageID + System.IO.Path.GetExtension(uploadedFile.FileName);
                string savePath = Server.MapPath(Constants.GalleryImageSettings.SaveImagePath.Description() + saveName);
                string file_ext = System.IO.Path.GetExtension(uploadedFile.FileName).ToUpper();

                if (file_ext == ".JPG" || file_ext == ".PNG" || file_ext == ".GIF")
                {
                    uploadedFile.SaveAs(savePath);
                }
                return Content(Url.Content(Constants.GalleryImageSettings.SaveImagePath.Description() + saveName));
            }
            else
            {
                return RedirectToAction("ManageGallery");
            }
        }
        #endregion
    }
    /// <summary>
    /// A custom version of the <see cref=\"AuthorizeAttribute\"/> that supports working
    /// around a cookie/session bug in Flash.  
    /// </summary>
    /// <remarks>
    /// Details of the bug and workaround can be found on this blog:
    /// http://geekswithblogs.net/apopovsky/archive/2009/05/06/working-around-flash-cookie-bug-in-asp.net-mvc.aspx
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class TokenizedAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// The key to the authentication token that should be submitted somewhere in the request.
        /// </summary>
        private const string TOKEN_KEY = "AuthenticationToken";

        /// <summary>
        /// This changes the behavior of AuthorizeCore so that it will only authorize
        /// users if a valid token is submitted with the request.
        /// </summary>
        /// <param name=\"httpContext\"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            string token = httpContext.Request.Params[TOKEN_KEY];

            if (token != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(token);

                if (ticket != null)
                {
                    FormsIdentity identity = new FormsIdentity(ticket);
                    string[] roles = System.Web.Security.Roles.GetRolesForUser(identity.Name);
                    GenericPrincipal principal = new GenericPrincipal(identity, roles);
                    httpContext.User = principal;
                }
            }

            return base.AuthorizeCore(httpContext);
        }
    }
}