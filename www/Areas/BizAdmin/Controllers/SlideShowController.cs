﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{

    [Authorize(Roles = "Admin")]
    public class SlideShowController : Controller
    {
        // GET: BizAdmin/SlideShow
        public ActionResult Index()
        {
            List<int> slideShow_ImgCount = DefaultConnectionDB.GetInstance().Query<int>("SELECT count([SlideShowID]) As PhotoCount FROM [dbo].[SlideShowImages] GROUP BY [SlideShowID]").ToList();

            
            List<SlideShow> getSlideshows = DefaultConnectionDB.GetInstance().Fetch<SlideShow>(PetaPoco.Sql.Builder
                 .Append("SELECT SlideShows.*, [Filename] as PrimaryImageUrl, Title,Caption from [SlideShowImages]")
                 .Append("RIGHT JOIN SlideShows ON SlideShows.SlideShowID = [SlideShowImages].SlideShowID")).ToList();

            ViewBag.Slideshows = getSlideshows;
            ViewBag.SlideshowCategories = getSlideshows.GroupBy(x => x.SlideShowID).Select(x => x.Key).ToList();
            return View();
        }


        #region Slideshow Methods

        public ActionResult EditSlideShow(int id)
         {
             SlideShow s = SlideShow.Single(id);

             return View("EditSlideShow",s);
         }

         public ActionResult CreateSlideShow()
         {
             SlideShow s = new SlideShow();
             return View("EditSlideShow",s);
         }

        
        public ActionResult DisplaySlideShow(int id)
        {
            var slideShow = SlideShow.SingleOrDefault("WHERE SlideShowId = @0", id);


            if (slideShow != null)
            {
                ViewBag.SlideShowName = slideShow.SlideShowName;
                ViewBag.slideShowID = slideShow.SlideShowID;

                List<SlideShowImage> images = SlideShowImage.Fetch("WHERE SlideShowID=@0 ORDER BY SortOrder", slideShow.SlideShowID);
                return View(images);
            }
            else
            {
                return RedirectToAction("Index", "SlideShow");
            }
        }

        [HttpPost]
        public ActionResult SaveSlideShow(SlideShow objSlideshow)
        {

            
            objSlideshow.Save();
            return Json(new { HasError = false, SlideShowName = objSlideshow.SlideShowName }, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        public ActionResult EditSlideShow1(int id)
        {
            SlideShow objSlideshow = SlideShow.SingleOrDefault("WHERE SlideShowID=@0", +id);
            return Json(new { HasError = false, SlideShow = objSlideshow }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteSlideShow(int slideShowId)
        {
            if (slideShowId != 0)
            {
                //delete the slideshow & all slideshow images 
                DefaultConnectionDB.GetInstance().Execute("DELETE FROM SlideShowImages WHERE SlideShowID=" + slideShowId);
                DefaultConnectionDB.GetInstance().Execute("DELETE FROM SlideShows WHERE SlideShowID=" + slideShowId);
                return Json(new { HasError = false });
            }
            else
            { return Json(new { HasError = true }); }
        }
        [AllowAnonymous]
        public ActionResult UploadSlideshow(HttpPostedFileBase fileData, string slideShowId)
        {
            if (fileData != null)
            {
                ViewBag.nextSlideShowId = DefaultConnectionDB.GetInstance().Query<int>("select max([SlideShowID]) + 1 from [dbo].[SlideShows]").FirstOrDefault();
                ViewBag.nextImageId = DefaultConnectionDB.GetInstance().Query<int>("select max([SlideShowImageID]) from [dbo].[SlideShowImages]").FirstOrDefault();

                var uploadedFile = Request.Files["filedata"];
                string imageName = uploadedFile.FileName.Split('.')[0].ToString() + "_slideShowId" + ViewBag.nextSlideShowId + "_" + Convert.ToInt32(ViewBag.nextImageId) + System.IO.Path.GetExtension(uploadedFile.FileName);
                string savePath = Server.MapPath(Constants.SlideshowSettings.UploadSlideshow_filePath.Description() + imageName.Split('.')[0] + System.IO.Path.GetExtension(uploadedFile.FileName));
                string file_ext = System.IO.Path.GetExtension(uploadedFile.FileName).ToUpper();

                if (file_ext == ".JPG" || file_ext == ".PNG" || file_ext == ".GIF")
                {
                    uploadedFile.SaveAs(savePath);
                }
                return Content(Url.Content(Constants.SlideshowSettings.UploadSlideshow_filePath.Description() + imageName));
            }
            else
            {
                return RedirectToAction("ManageGallery");
            }
        }
        #endregion

        #region SlideshowImage Methods
        [HttpPost]
        public ActionResult saveSlideshowImage(SlideShowImage slide, HttpPostedFileBase image)
        {
            SlideShowImage slideShowImage = slide;
            
            string fname = slideShowImage.Title + "_" + slideShowImage.SlideShowImageID.ToString() + ".png";
            string fpath = Path.Combine(Server.MapPath(Constants.SlideshowSettings.UploadSlideshow_filePath.Description() + fname));

            string cfpath;
            
            cfpath = Path.Combine(Constants.SlideshowSettings.SaveCropImage_filePath.Description(), fname);

            image.SaveAs(Server.MapPath(cfpath));
            slide.Filename = cfpath;
            slide.Save();

            


            /*
             * if (objSlideshowImage.Filename != null)
            {
                string fpath = Path.Combine(Server.MapPath(Constants.SlideshowSettings.UploadSlideshow_filePath.Description() + objSlideshowImage.Filename.Split('/').Last()));
                Image oimg = Image.FromFile(fpath);
                Image img = imgresize(oimg);
                Rectangle cropcords = new Rectangle(
                Decimal.ToInt32(Convert.ToDecimal(x)),
                Decimal.ToInt32(Convert.ToDecimal(y)),
                Convert.ToInt32(objSlideshowImage.Width),
                Convert.ToInt32(objSlideshowImage.Height));
                string imageName = saveCropImage(cropcords, img, objSlideshowImage.Filename.Split('/').Last());

                objSlideshowImage.Filename = Constants.SlideshowSettings.SaveCropImage_filePath.Description() + imageName;
                slideShowImage.SlideShowID = objSlideshowImage.SlideShowID;
                slideShowImage.Height = objSlideshowImage.Height;
                slideShowImage.Width = objSlideshowImage.Width;
                slideShowImage.Filename = objSlideshowImage.Filename;
            }
 * */

            return Json(new { HasError = false, slide }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddSlideShowImage(int id)
        {
            SlideShowImage objSlideshowImage = new SlideShowImage();

            SlideShow ss = SlideShow.SingleOrDefault("WHERE SlideShowID=@0", +id);
            ViewBag.SlideShowWidth = ss.Width;
            ViewBag.SlideShowHeight = ss.Height;
            objSlideshowImage.SlideShowID = id;
            objSlideshowImage.Width = ss.Width;
            objSlideshowImage.Height = ss.Height;

            return View("EditSlideShowImage", objSlideshowImage);

        }

        
        public ActionResult EditSlideShowImage(int id)
        {
            SlideShowImage objSlideshowImage;
            objSlideshowImage = SlideShowImage.SingleOrDefault("WHERE SlideShowImageID=@0", +id);
            SlideShow ss = SlideShow.SingleOrDefault("WHERE SlideShowID=@0", objSlideshowImage.SlideShowID);
            ViewBag.SlideShowWidth = ss.Width;
            ViewBag.SlideShowHeight = ss.Height;
            return View("EditSlideShowImage",objSlideshowImage);

        }

        [HttpPost]
        public ActionResult EditSlideShowImage1(int imgid)
        {
            SlideShowImage objSlideshowImage = SlideShowImage.SingleOrDefault("WHERE SlideShowImageID=@0", +imgid);
            return Json(new { HasError = false, SlideShowImg = objSlideshowImage }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CancelUploadedSlideShowImage(string uploadedPath, string slideShowName)
        {
            if (uploadedPath != null)
            {
                var Path = Server.MapPath(Constants.SlideshowSettings.UploadSlideshow_filePath.Description() + uploadedPath.Split('/').Last());
                if (System.IO.File.Exists(Path))
                {
                    System.IO.File.Delete(Path);
                }
            }
            return Json(new { HasError = false, SlideShowName = slideShowName }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSlideShowImage(int imgid)
        {
            if (imgid != 0)
            {
                DefaultConnectionDB.GetInstance().Execute("DELETE FROM SlideShowImages WHERE SlideShowImageID=" + imgid);
                return Json(new { HasError = false });
            }
            else
            { return Json(new { HasError = true }); }
        }

        #endregion

        #region Helper Methods
        [NonAction]
        public Bitmap imgresize(Image img)
        {
            Bitmap bitMap = new Bitmap(450, 300, img.PixelFormat);
            Graphics grph = Graphics.FromImage(bitMap);
            grph.DrawImage(img, new Rectangle(0, 0, bitMap.Width, bitMap.Height), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            return bitMap;
        }
        [NonAction]
        public string saveCropImage(Rectangle cropcords, Image source_image, string fname)
        {
            string cfname, cfpath;

            cfname = "crop_" + fname;
            cfpath = Path.Combine(Server.MapPath(Constants.SlideshowSettings.SaveCropImage_filePath.Description()), cfname);

            if (cropcords.Height > 0 && cropcords.Width > 0 && cropcords.Height<= source_image.Height && cropcords.Width <= source_image.Width)
            {
                Bitmap bmp = new Bitmap(cropcords.Width, cropcords.Height, source_image.PixelFormat);
                Graphics grph = Graphics.FromImage(bmp);
                grph.DrawImage(source_image, new Rectangle(0, 0, bmp.Width, bmp.Height), cropcords, GraphicsUnit.Pixel);
                bmp.Save(cfpath);
            }
            else {
                cropcords.Height = source_image.Height;
                cropcords.Width = source_image.Width;
                source_image.Save(cfpath);

            }
            return cfname;
        }
        #endregion
    }
}