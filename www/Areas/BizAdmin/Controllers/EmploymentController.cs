﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biznetix.BizCMS;
using Biznetix.Common.StringExtensions;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{    
    public class EmploymentController : Controller
    {
        // GET: BizAdmin/Employment
        public ActionResult Index()
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                return View("FeatureDisabled");
            }
            return View();
        }

        public ActionResult Applications(int? id)
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                return View("FeatureDisabled");
            }
            ViewBag.job_id = id;
            return View();
        }

        public ActionResult Applicant(int id)
        {
            var enabledInConfig = GlobalSettings.config_settings["EnableEmployment"].ToBoolean();
            if (!enabledInConfig)
            {
                return View("FeatureDisabled");
            }
            ViewBag.EmploymentAppBackgroundInfo = GlobalSettings.config_settings["EmploymentAppBackgroundInfo"].ToBoolean();
            ViewBag.EmploymentAppCollectSSN = GlobalSettings.config_settings["EmploymentAppCollectSSN"].ToBoolean();
            ViewBag.EmploymentAppEducationHistory = GlobalSettings.config_settings["EmploymentAppEducationHistory"].ToBoolean();
            ViewBag.EmploymentAppEmploymentHistory = GlobalSettings.config_settings["EmploymentAppEmploymentHistory"].ToBoolean();
            ViewBag.EmploymentAppJobData = GlobalSettings.config_settings["EmploymentAppJobData"].ToBoolean();
            ViewBag.EmploymentAppPersonalData = GlobalSettings.config_settings["EmploymentAppPersonalData"].ToBoolean();
            ViewBag.EmploymentAppReferences = GlobalSettings.config_settings["EmploymentAppReferences"].ToBoolean();

            ViewBag.OpenJobs = Job_Listing.Fetch("");
            ViewBag.Locations = Job_Location.Fetch("");

            Job_Application application = Job_Application.SingleOrDefault(id);
            return View(application);
        }

        
        #region CRUD Methods
        public ActionResult Create()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEmployment'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "Job_ID")]Job_Listing joblist)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEmployment'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                DefaultConnectionDB.GetInstance().Insert("Job_Listings", "Job_ID", joblist);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEmployment'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            Job_Listing jobList = Job_Listing.Single(id);
            return View(jobList);
        }

        [HttpPost]
        public ActionResult Edit(Job_Listing joblist)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEmployment'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                joblist.Update();                
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        #endregion
    }
}