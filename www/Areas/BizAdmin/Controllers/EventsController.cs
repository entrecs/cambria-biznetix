﻿using Biznetix.BizCMS.Data;
using Biznetix.Common.StringExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventsController : Controller
    {
        // GET: BizAdmin/Events
        public ActionResult Index()
        {
            var enabledInConfig = GlobalSettings.config_settings.ContainsKey("EnableEvents") && GlobalSettings.config_settings["EnableEvents"].ToBoolean();
            if (!enabledInConfig)
            {
                return View("FeatureDisabled");
            }
            List<Event> events = Event.Fetch("");
            Session["Events"] = events;
            return View();
        }
        #region CRUD Methods
        public ActionResult Create()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEvents'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            IEnumerable<Biznetix.BizCMS.Data.EventCategory> eventCategories = EventCategory.Fetch("ORDER BY CATEGORYNAME ASC");
            ViewBag.EventCategory = eventCategories;
            ViewBag.EventStartTime = "12:00 AM";
            ViewBag.EventEndTime = "12:00 PM";
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "EventID")]Event newEvent)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEvents'").Value;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                newEvent.EventStartDate = newEvent.EventStartDate.Value.Date + newEvent.EventStartTime.Value.TimeOfDay;
                newEvent.EventEndDate = newEvent.EventEndDate.Value.Date + newEvent.EventEndTime.Value.TimeOfDay;

                DefaultConnectionDB.GetInstance().Insert("Events", "EventID", newEvent);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEvents'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            IEnumerable<Biznetix.BizCMS.Data.EventCategory> eventCategories = EventCategory.Fetch("ORDER BY CATEGORYNAME ASC");
            ViewBag.EventCategory = eventCategories;

            IEnumerable<SelectListItem> Status = new List<System.Web.Mvc.SelectListItem>() 
            { 
               new SelectListItem { Value = "-1", Text = "NotSet" }, 
               new SelectListItem { Value = "1", Text = "True" },
               new SelectListItem { Value = "0", Text = "False" }               
            };
            ViewBag.Status = Status;
            Event events = Event.Single(id);
            events.EventStartTimeValue = events.EventStartDate.HasValue ? events.EventStartDate.Value.ToString("t") : string.Empty;
            events.EventEndTimeValue = events.EventEndDate.HasValue ? events.EventEndDate.Value.ToString("t") : string.Empty;
            ViewBag.EventStartTime = events.EventStartTime;
            ViewBag.EventEndTime = events.EventEndTime;
            //Event_Items items = new Event_Items(events);                      
            return View(events);
        }

        [HttpPost]
        public ActionResult Edit(Event events)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableEvents'").Value;; if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {

                events.EventStartDate = events.EventStartDate.Value.Date + events.EventStartTime.Value.TimeOfDay;
                events.EventEndDate = events.EventEndDate.Value.Date + events.EventEndTime.Value.TimeOfDay;
                events.Save();
                //eventItem.saveEvents(eventItem);
                return RedirectToAction("Index");
            }
            else
            {
                var errors = ModelState
                    .Where(x => x.Value.Errors.Count > 0)
                    .Select(x => new { x.Key, x.Value.Errors })
                    .ToArray();

                    // Breakpoint, Log or examine the list with Exceptions.

                return View();
            }
        }
        #endregion
    }
}