﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
        [Authorize(Roles = "Admin")]
    public class BizAdminController : Controller
    {               
        public ActionResult Index()
        {
            var user = User.Identity.Name;
            var userId = AspNetUser.Fetch("").Where(x => x.UserName == User.Identity.Name).Select(y => y.Id).SingleOrDefault();
            bool isAuth = DepartmentUser.Fetch("").Where(x => x.DepartmentTypeId == 38 && x.UserId == userId).Any();
            ViewData["userAuthorized"] = isAuth;  
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ActionResult ConfigurationSettings()
        {
            List<configuration_setting> settings = configuration_setting.Fetch("").OrderBy(x => x.Order).ToList();
            ViewBag.Types = settings.Select(x => x.Type).Distinct().ToList();
            ViewBag.Groups = settings.Select(x => x.Group).Distinct().ToList();
            return View(settings);
        }
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost]
        public ActionResult ConfigurationSettings(List<configuration_setting> settings)
        {
            if (settings != null)
            {
                foreach (var config in settings)
                {
                    configuration_setting setting = configuration_setting.SingleOrDefault((object)config.Key);
                    
                    setting.Value = config.Value;
                    setting.Type = config.Type;
                    setting.Update();
                }
                GlobalSettings.LoadSettings();
                return Json(new { Message = "Settings are successfully saved." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Message = "No changes detected." }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult AddConfigurationSettings()
        {            
            return View();
        }
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost]
        public ActionResult AddConfigurationSettings(List<configuration_setting> settings)
        {
            configuration_setting setting = new configuration_setting();
            foreach (var config in settings)
            {
                setting.Group = config.Group;
                setting.Value = config.Value;
                setting.Type = config.Type;
                setting.Key = config.Key;
                setting.Insert();
            }
            GlobalSettings.LoadSettings();
            return Json(new { Message = "Successfully added new config setting." }, JsonRequestBehavior.AllowGet);
        }
    }
}