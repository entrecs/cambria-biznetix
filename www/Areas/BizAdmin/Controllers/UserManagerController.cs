﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserManagerController : Controller
    {

        // GET: BizAdmin/UserManager
        public ActionResult Index()
        {
            return View();
        }
        #region CRUD Methods
        public ActionResult Create()
        {
            List<Department> DepartmentTypeList = Department.Fetch("");           
            ViewBag.AdminRole = false;
            ViewBag.SuperAdminRole = false;
            ViewBag.TrustedRole = false;
            ViewBag.Deparments = DepartmentTypeList;
            return View();
        }
        [HttpPost]
        public ActionResult Create(RegisterViewModel users, string departments, bool AdminRole)
        {
            if (ModelState.IsValid)
            {
                Biznetix.BizCMS.Models.IdentityManager _identityUser = new Models.IdentityManager();
                Models.ApplicationUser appUser = new Models.ApplicationUser
                {
                    UserName = users.UserName,
                    LastName = users.LastName,
                    FirstName = users.FirstName,
                    EmailConfirmed = true,
                    Email = users.Email,
                    CreatedDateTime = DateTime.Now
                };
                bool result = _identityUser.CreateUser(appUser, users.Password);

                if (result)
                {
                    AspNetUser objUser = AspNetUser.Single("Where UserName=@0", users.UserName);
                    if (AdminRole)
                    {
                        Biznetix.BizCMS.Data.AspNetUserRole role = new AspNetUserRole();
                        role.RoleId = "1";
                        role.UserId = objUser.Id;
                        role.Insert();
                    }
                    if (!string.IsNullOrEmpty(departments))
                    {
                        departments = departments.Remove(departments.Length - 1).ToString();
                        string[] departmentLst = departments.Split(',');
                        foreach (var item in departmentLst)
                        {
                            DepartmentUser departmentUsr = new DepartmentUser();
                            departmentUsr.DepartmentName = item.Split('_')[0].ToString();
                            departmentUsr.DepartmentTypeId = Convert.ToInt32(item.Split('_')[1].ToString());
                            departmentUsr.UserId = objUser.Id.ToString();
                            departmentUsr.IsAssigned = true;
                            departmentUsr.Save();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        public ActionResult Edit(string userName)
        {
            List<Department> DepartmentTypeList = Department.Fetch("");
            Biznetix.BizCMS.Data.AspNetUser user = AspNetUser.Fetch("Where UserName=@0", userName).FirstOrDefault();
            List<DepartmentUser> DepartmentUsrList = DepartmentUser.Fetch("Where UserId=@0", user.Id);
            ViewBag.DeparmentUser = DepartmentUsrList;
            ViewBag.Departments = DepartmentTypeList;
            ViewBag.AdminRole = AspNetUserRole.Fetch("").Where(x => x.UserId == user.Id && x.RoleId.Equals("1")).Any();
            ViewBag.SuperAdminRole = AspNetUserRole.Fetch("").Where(x => x.UserId == user.Id && x.RoleId.Equals("2")).Any();
            ViewBag.TrustedRole = AspNetUserRole.Fetch("").Where(x => x.UserId == user.Id && x.RoleId.Equals("3")).Any();
            ViewBag.Password = user.PasswordHash;
         
            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(AspNetUser users, string departments, bool AdminRole, bool SuperAdminRole, bool TrustedRole)
        {
            if (ModelState.IsValid)
            {
                AspNetUser objUser = AspNetUser.Single("Where UserName=@0", users.UserName);
                objUser.UserName = users.UserName;
                objUser.FirstName = users.FirstName;
                objUser.LastName = users.LastName;
                objUser.Email = users.Email;
                objUser.Update();
                bool[] roles = { AdminRole, SuperAdminRole, TrustedRole }; // TODO: pass these in as an array (or string?)
                for (var i = 1; i <= roles.Length; i++) {
                    if (roles[i - 1])
                    {
                        if (!AspNetUserRole.Fetch("").Where(x => x.RoleId.Equals(i.ToString()) && x.UserId == objUser.Id).Any())
                        {
                            Biznetix.BizCMS.Data.AspNetUserRole role = new AspNetUserRole();
                            role.RoleId = i.ToString();
                            role.UserId = objUser.Id;
                            var result = role.Insert();
                        }
                    }
                    else
                    {
                        AspNetUserRole del_UsrRole = AspNetUserRole.Fetch("").Where(x => x.UserId == objUser.Id && x.RoleId.Equals(i.ToString())).FirstOrDefault();
                        if (del_UsrRole != null)
                        {
                            AspNetUserRole.Delete("WHERE [UserId] = @0 AND [RoleId] = @1", objUser.Id, i.ToString());
                        }
                    }
                }
                //if (!AdminRole)
                //{
                //string[] departmentLst = departments.Remove(departments.Length - 1).Split(',');
                //foreach (var item in departmentLst)
                //{
                //    DepartmentUser departmentUsr = DepartmentUser.Fetch("Where DepartmentTypeId=@0 and UserId=@1", item.Split('|')[1].ToString(), objUser.Id).FirstOrDefault();
                //    if (departmentUsr != null)
                //    {
                //        departmentUsr.IsAssigned = item.Split('|')[0].ToString() == "Assigned" ? true : false;
                //        departmentUsr.Update();
                //    }
                //    else
                //    {
                //        if (item.Split('|')[0].ToString() == "Assigned")
                //        {
                //            Department department = Department.Fetch("Where DepartmentTypeId=@0", item.Split('|')[1].ToString()).FirstOrDefault();
                //            DepartmentUser deptUsr = new DepartmentUser();
                //            deptUsr.DepartmentName = department.DepartmentName;
                //            deptUsr.DepartmentTypeId = department.DepartmentTypeId;
                //            deptUsr.UserId = objUser.Id;
                //            deptUsr.IsAssigned = item.Split('|')[0].ToString() == "Assigned" ? true : false;
                //            deptUsr.Save();
                //        }
                //    }
                //}
                //}
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
        #endregion
    }
}