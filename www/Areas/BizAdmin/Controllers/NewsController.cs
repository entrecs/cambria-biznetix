﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Areas.BizAdmin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NewsController : Controller
    {
        // GET: BizAdmin/News
        public ActionResult Index()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            List<NewsItem> newsitems = NewsItem.Fetch("");
            Session["NewsItems"] = newsitems;
            return View(newsitems);
        }
        public ActionResult Create()
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Exclude = "NewsItem_ID")]NewsItem newsItem)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                newsItem.Save();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            return View(NewsItem.Single(id));
        }

        [HttpPost]
        public ActionResult Edit(NewsItem newsItem)
        {
            var enabledInConfig = configuration_setting.SingleOrDefault("WHERE [Key] = 'EnableNews'").Value;;
            if (!enabledInConfig.Equals("True") && !enabledInConfig.Equals("1"))
            {
                return View("FeatureDisabled");
            }
            if (ModelState.IsValid)
            {
                newsItem.Save();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
    }
}