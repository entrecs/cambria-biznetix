﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class NewsController : ApiController
    {
        // GET api/news
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<NewsItem> queryResults = Core.DataUtils.PagedQuery<NewsItem>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        public NewsItem Get(int id)
        {
            return NewsItem.Single(id);
        }

        // DELETE api/news/5
        public void Delete(int id)
        {
            NewsItem.Delete(id);
        }      
    }
}
