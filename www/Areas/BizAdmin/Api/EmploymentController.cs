﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class EmploymentController : ApiController
    {
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<Job_Listing> queryResults = Core.DataUtils.PagedQuery<Job_Listing>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        // GET api/ListApplicants/5
        public HttpResponseMessage ListApplicants([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<Job_Application> queryResults = Core.DataUtils.PagedQuery<Job_Application>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        // GET api/Job_Listing/5
        public Job_Listing Get(int id)
        {
            return Job_Listing.Single(id);
        }

        // DELETE api/Delete/5
        public void Delete(int id)
        {
            Job_Listing.Delete(id);
        }
    }
}
