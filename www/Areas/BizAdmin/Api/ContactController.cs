﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class ContactController : ApiController
    {
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<ContactSubmission> queryResults = Core.DataUtils.PagedQuery<ContactSubmission>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public ContactSubmission Get(int id)
        {
            return ContactSubmission.Single(id);
        }
        public void Delete(int id)
        {
            if (id != 0)
            {
                ContactSubmission.Delete(id);
            }
        }
    }
}
