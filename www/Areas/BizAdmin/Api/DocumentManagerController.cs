﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class DocumentManagerController : ApiController
    {
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<FST_Folder> queryResults = Core.DataUtils.PagedQuery<FST_Folder>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        public FST_Folder Get(int id)
        {
            return FST_Folder.Single(id);
        }

        // DELETE api/news/5        
        public void Delete(int folderid)
        {
            string folderdetails = "";
            int id = Convert.ToInt32(folderdetails.Split('_')[0].ToString());
            string sourcePath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), folderdetails.Split('_')[0].ToString(), folderdetails.Split('_')[1].ToString()));
            if (Directory.Exists(sourcePath))
            {
                Directory.Delete("");
            }
            FST_Folder.Delete(id);
        }
    }
}
