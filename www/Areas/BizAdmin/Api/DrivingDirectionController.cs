﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class DrivingDirectionController : ApiController
    {
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<CompanyLocation> queryResults = Core.DataUtils.PagedQuery<CompanyLocation>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        public CompanyLocation Get(int id)
        {
            return CompanyLocation.Single(id);
        }
        public void Delete(int id)
        {
            if (id != 0)
            {
                CompanyLocation.Delete(id);
            }
        }
    }
}
