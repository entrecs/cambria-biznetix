﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class FolderManagerController : ApiController
    {

        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<FST_Folder> queryResults = Core.DataUtils.PagedQuery<FST_Folder>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        [HttpGet]
        public FST_Folder Get(int id)
        {
            return FST_Folder.Single(id);
        }
        // DELETE api/news/5     
        [HttpDelete]
        public void Delete(int id)
        {
            FST_Folder docfolder = Get(id);
            FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", docfolder.FolderID).FirstOrDefault();
            //delete files in the folder
            List<FST_File> files = FST_File.Fetch("Where FolderID=@0", docfolder.FolderID).ToList();
            foreach (var item in files)
            {
                string file_sourcePath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("{0}{1}_{2}\\{3}", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName, Selectedfolder.FolderID, item.FileName));
                if (File.Exists(file_sourcePath))
                {
                    File.Delete(file_sourcePath);
                }
                FST_File.Delete(item.FileID);
            }

            string sourcePath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("{0}{1}_{2}", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName.ToString(), Selectedfolder.FolderID.ToString().ToString()));
            if (Directory.Exists(sourcePath))
            {
                Directory.Delete(sourcePath, true);
            }
            FST_Folder.Delete(id);
        }
    }
}
