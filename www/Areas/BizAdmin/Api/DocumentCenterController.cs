﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class DocumentCenterController : ApiController
    {
        public HttpResponseMessage Get(int id, [FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<FST_File> queryResults = Core.DataUtils.PagedQuery<FST_File>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items.Where(x => x.FolderID == id).ToList(), (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
