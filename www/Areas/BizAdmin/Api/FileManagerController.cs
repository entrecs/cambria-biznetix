﻿using Biznetix.BizCMS.Data;
using Biznetix.BizCMS.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class FileManagerController : ApiController
    {        
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest, bool active)
        {
            PetaPoco.Page<FST_File> queryResults = Core.DataUtils.PagedQuery<FST_File>(dtRequest);
            if (!active)
            {
                queryResults.Items = queryResults.Items.Where(x => x.Active == true).ToList();
            }
            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);            
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        public FST_File Get(int id)
        {
            return FST_File.Single(id);
        }
        [HttpDelete]
        public void Delete(int id)
        {
            if (id != 0)
            {
                FST_File docfile = Get(id);
                FST_Folder Selectedfolder = FST_Folder.Fetch("Where FolderID=@0", docfile.FolderID).FirstOrDefault();
                string sourcePath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("{0}{1}_{2}\\{3}", Constants.DocumentPathSettings.DirectoryPath.Description(), Selectedfolder.FolderName, Selectedfolder.FolderID, docfile.FileName));
                if (File.Exists(sourcePath))
                {
                    File.Delete(sourcePath);
                }
                FST_File.Delete(id);
            }
        }
    }
}
