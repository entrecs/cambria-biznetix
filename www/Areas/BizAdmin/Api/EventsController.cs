﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{   
    public class EventsController : ApiController
    {
       //  private const string MyRoutePrefix = "api";
          [HttpGet]
        // GET api/events
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {
            PetaPoco.Page<Event> queryResults = Core.DataUtils.PagedQuery<Event>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        public Event Get(int id)
        {
            return Event.Single(id);
        }
        public void Delete(int id)
        {
            if (id != 0)
            {
                Event.Delete(id);
            }
        }
    }
}
