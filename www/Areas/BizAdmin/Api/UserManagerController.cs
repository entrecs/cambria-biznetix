﻿using Biznetix.BizCMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Biznetix.BizCMS.Areas.BizAdmin.Api
{
    public class UserManagerController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get([FromUri]Core.DataTables.DataTablesRequest dtRequest)
        {

            PetaPoco.Page<AspNetUser> queryResults = Core.DataUtils.PagedQuery<AspNetUser>(dtRequest);

            Core.DataTables.DataTablesResponse response = new Core.DataTables.DataTablesResponse(dtRequest.Draw, queryResults.Items, (int)queryResults.TotalItems, (int)queryResults.TotalItems);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        // GET api/news/5
        //public AspNetUser Get(string userName)
        //{
        //    return AspNetUser.Single(userName);
        //}

        public void Delete(string id)
        {
            // username is base64 encoded to avoid issues with special characters, e.g. the @ symbol
            AspNetUser.Delete("Where UserName=@0", System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(id)));
        }

    }
}
