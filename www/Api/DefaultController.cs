﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using Biznetix.BizCMS.Helpers;
using Biznetix.BizCMS.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;

namespace Biznetix.BizCMS.Api
{
    public class DefaultController : ApiController
    {

        // POST api/values
        public HttpResponseMessage Post([FromBody]JToken formJson)
        {
            String mail_body = "";
            String field_format = "<b>{0}</b>: {1}<br/>";
            String emailfrom = configuration_setting.SingleOrDefault("WHERE [Key] = 'FormEmailFrom'").Value;
            String emailTo = configuration_setting.SingleOrDefault("WHERE [Key] = 'FormEmailTo'").Value;
            foreach (JProperty property in formJson.Children()) {
                // TODO: If we have recursive fields this is gonna go poorly
                switch (property.Name)
                {
                    case "email":
                        emailfrom = property.Value.ToString();
                        mail_body += String.Format(field_format, "Email", property.Value.ToString());
                        break;
                    case "name":
                    case "theirname":
                        mail_body += String.Format(field_format, "Name", property.Value.ToString());
                        break;
                    default:
                        mail_body += String.Format(field_format, property.Name, property.Value.ToString());
                        break;
                }
                
            }
            SendEmail.Send(emailTo, mail_body, "Remember the Date Form Submitted on " + DateTime.Now.ToShortDateString(), emailfrom, "");

            var response = new HttpResponseMessage(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri(Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority + "/form-submitted");

            return response;
        }

    }
}