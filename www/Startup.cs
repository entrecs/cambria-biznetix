﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(Biznetix.BizCMS.Startup))]
namespace Biznetix.BizCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            System.Web.Mvc.GlobalFilters.Filters.Add(new Helpers.Filters.NavbarFilter());
        }
    }
}
