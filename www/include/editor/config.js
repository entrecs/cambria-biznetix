/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.removeButtons = 'Subscript,Superscript,Anchor,Print,NewPage,Preview,Templates';
    config.removeDialogTabs = 'link:upload;image:Upload';
    // Se the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    config.removePlugins = 'elementspath,save'
    //config.filebrowserImageBrowseUrl = '/Home/uploadPartial';
    //config.filebrowserImageUploadUrl = '/Home/Upload';
    config.filebrowserImageBrowseUrl = window.CKEDITOR_BASEPATH + "ImageBrowser.aspx";
    config.filebrowserImageWindowWidth = 780;
    config.filebrowserImageWindowHeight = 720;
    config.filebrowserBrowseUrl = window.CKEDITOR_BASEPATH + "LinkBrowser.aspx";
    config.filebrowserWindowWidth = 500;
    config.filebrowserWindowHeight = 650;
    config.coreStyles_bold = {
        element: 'strong',
        overrides: 'b',
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.coreStyles_italic = {
        element: 'em',
        overrides: 'i',
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.coreStyles_underline = {
        element: 'span',
        styles: { textDecoration: 'underline' },
        attributes: { 'class': 'Underline' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.colorButton_foreStyle = {
        element: 'span',
        styles: { color: '#(color)' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.colorButton_backStyle = {
        element: 'span',
        styles: { 'background-color': '#(color)' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.fontSize_style = {
        element: 'span',
        styles: { 'font-size': '#(size)' },
        overrides: [{ element: 'font', attributes: { 'size': null } }],
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.font_style = {
        element: 'span',
        styles: { 'font-family': '#(family)' },
        overrides: [{ element: 'font', attributes: { 'face': null } }],
        childRule: function (element) {
            return !element.is('a');
        }
    };


	//config.extraAllowedContent = 'dl dt dd iframe';
	config.allowedContent = true;
	
    config.extraPlugins = 'savebtn';
    config.saveSubmitURL = '/BizAdmin/Pages/UpdateContent';

	};
