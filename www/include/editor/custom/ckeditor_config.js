﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.stylesSet.add('simple_styles', [
    { name: 'Big', element: 'big' },
    { name: 'Small', element: 'small' },
    { name: 'Typewriter', element: 'tt' },

    { name: 'Computer Code', element: 'code' },
    { name: 'Keyboard Phrase', element: 'kbd' },
    { name: 'Sample Text', element: 'samp' },
    { name: 'Variable', element: 'var' },

    { name: 'Deleted Text', element: 'del' },
    { name: 'Inserted Text', element: 'ins' },

    { name: 'Cited Work', element: 'cite' },
    { name: 'Inline Quotation', element: 'q' },
]);

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.removeButtons = 'Subscript,Superscript,Anchor,Print,NewPage,Preview';
    config.removeDialogTabs = 'link:upload;image:Upload';
    // Se the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    config.removePlugins = 'elementspath,save,flash,btquicktable,btgrid,scayt,wsc,forms,bidi,language,blockquote'; // btquicktable seems to have a broken dependency
    //config.filebrowserImageBrowseUrl = '/Home/uploadPartial';
    //config.filebrowserImageUploadUrl = '/Home/Upload';
    config.filebrowserImageBrowseUrl = window.CKEDITOR_BASEPATH + "ImageBrowser.aspx";
    config.filebrowserImageWindowWidth = 780;
    config.filebrowserImageWindowHeight = 720;
    config.filebrowserBrowseUrl = window.CKEDITOR_BASEPATH + "LinkBrowser.aspx";
    config.filebrowserWindowWidth = 500;
    config.filebrowserWindowHeight = 650;
    config.stylesSet = 'simple_styles';
    config.coreStyles_bold = {
        element: 'strong',
        overrides: 'b',
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.coreStyles_italic = {
        element: 'em',
        overrides: 'i',
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.coreStyles_underline = {
        element: 'span',
        styles: { textDecoration: 'underline' },
        attributes: { 'class': 'Underline' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.colorButton_foreStyle = {
        element: 'span',
        styles: { color: '#(color)' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.colorButton_backStyle = {
        element: 'span',
        styles: { 'background-color': '#(color)' },
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.fontSize_style = {
        element: 'span',
        styles: { 'font-size': '#(size)' },
        overrides: [{ element: 'font', attributes: { 'size': null } }],
        childRule: function (element) {
            return !element.is('a');
        }
    };
    config.font_style = {
        element: 'span',
        styles: { 'font-family': '#(family)' },
        overrides: [{ element: 'font', attributes: { 'face': null } }],
        childRule: function (element) {
            return !element.is('a');
        }
    };

    config.templates_files = ['/include/editor/plugins/templates/templates/uw-templates.js'];
    config.templates_replaceContent = false;

    //config.extraAllowedContent = 'dl dt dd iframe';
    config.allowedContent = true;

    config.extraPlugins = 'savebtn';
    config.saveSubmitURL = '/BizAdmin/Pages/UpdateContent';

};
