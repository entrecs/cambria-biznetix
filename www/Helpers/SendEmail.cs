﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace Biznetix.BizCMS.Helpers
{
    public static class SendEmail
    {
        public static void Send(string to,
                         string body,
                         string subject,
                         string fromAddress,
                         string fromDisplay)
        {
                        string host;
           try
            {
				host = ConfigurationManager.AppSettings["SMTPServer"];
            }
            catch
            {
                host = "mail.biznetix.net";
            }           
            try
            {
                MailMessage mail = new MailMessage();
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.To.Add(new MailAddress(to));
                mail.From = new MailAddress(fromAddress, fromDisplay, Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.Priority = MailPriority.Normal;
                SmtpClient smtp = new SmtpClient();
                if(!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPUser"]) && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPPassword"])) {
                    smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUser"], ConfigurationManager.AppSettings["SMTPPassword"]);
                }
                smtp.Host = host; /*Constants.EmailSettings.Mail_Host.Description();*/
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder(1024);
                sb.Append("\nTo:" + to);
                sb.Append("\nbody:" + body);
                sb.Append("\nsubject:" + subject);
                sb.Append("\nfromAddress:" + fromAddress);
                sb.Append("\nfromDisplay:" + fromDisplay);
                sb.Append("\nMessage:" + ex.Message);
                sb.Append("\nHosting:" + host);
                //ErrorLog(sb.ToString(), ex.ToString(), ErrorLogCause.EmailSystem);
                throw (ex);
            }
        }
    }
}