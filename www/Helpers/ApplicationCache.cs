﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Helpers
{
    public static class ApplicationCache
    {
        public static void Load()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlCacheDependencyAdmin.EnableNotifications(connectionString);            
            SqlCacheDependencyAdmin.EnableTableForNotifications(connectionString, "Events");
            SqlCacheDependencyAdmin.EnableTableForNotifications(connectionString, "NewsItems");
            EventsCache = Event.Fetch("").ToList();
            NewsCache = NewsItem.Fetch("").ToList();
        }

        public static void AddToCache(string cacheName, object value)
        {
            System.Web.HttpContext.Current.Cache.Insert(cacheName.ToString(), value, null,  DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public static object GetFromCache(string cacheName)
        {
            return System.Web.HttpContext.Current.Cache[cacheName.ToString()];
        }
        public static List<Event> EventsCache
        {
            get
            {
                if (GetFromCache("Events") != null)
                {
                    return (List<Event>)GetFromCache("Events");
                }
                else
                {
                    List<Event> listEvents = Event.Fetch("").ToList();
                    AddToCache("Events", listEvents);
                    return listEvents;
                }
            }
            set
            {
                AddToCache("Events", value);
            }
        }
        public static List<NewsItem> NewsCache
        {
            get
            {
                if (GetFromCache("NewsItems") != null)
                {
                    return (List<NewsItem>)GetFromCache("NewsItems");
                }
                else
                {
                    List<NewsItem> listEvents = NewsItem.Fetch("").ToList();
                    AddToCache("NewsItems", listEvents);
                    return listEvents;
                }
            }
            set
            {
                AddToCache("NewsItems", value);
            }
        }
    }
}