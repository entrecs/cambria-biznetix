﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Helpers.Filters
{
    public class NavbarFilter : FilterAttribute, IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            IEnumerable<Biznetix.BizCMS.Data.cms_Page> allpages = (IEnumerable<Biznetix.BizCMS.Data.cms_Page>)HttpRuntime.Cache["Allpages"];
            if (allpages == null)
            {
                allpages = cms_Page.Fetch("ORDER BY Sort, PageName");
                HttpRuntime.Cache["Allpages"] = allpages;
            }

            IEnumerable<Biznetix.BizCMS.Data.cms_Page> navpages = allpages.Where(x => x.Display == true);
            filterContext.Controller.ViewBag.Navpages = filterContext.Controller.ViewBag.navpages = navpages; // Apparently we use both capitalizations
            filterContext.Controller.ViewBag.Featuredpages = cms_Page.Fetch("WHERE Featured <> 0 AND Display <> 0 ORDER BY sort");
        }
    }
}
