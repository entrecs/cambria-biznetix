﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Biznetix.BizCMS.Helpers
{
    public class MailHelper
    {
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string RecipientCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public void SendMail()
        {
            SendEmail.Send(Recipient, Body, Subject, Sender, Sender);
        }
    }

}