﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Biznetix.BizCMS.Helpers
{
    public static class Constants
    {
        public enum SlideshowSettings
        {         
            [DescriptionAttribute("~/ContentPages/Slideshow/uploaded/")]
            UploadSlideshow_filePath,
            [DescriptionAttribute("/ContentPages/Slideshow/cropImages/")]
            SaveCropImage_filePath,
            [DescriptionAttribute("800 * 600")]
            SlideshowResolution
        }
        public enum GalleryImageSettings
        {
            [DescriptionAttribute("~/ContentPages/Gallery/")]
            SaveImagePath
        }
        public enum EditPage_TabSocialImage
        {
            [DescriptionAttribute("~/ContentPages/Editpages_Social/")]
            SaveImagePath
        }
        public enum DocumentPathSettings
        {
            [DescriptionAttribute("~/ContentPages/Documents/")]
            DirectoryPath,
            [DescriptionAttribute("~/ContentPages/Documents/SharedDocs/")]
            SharedDirectoryPath
        }

        public enum MIME
        {
            [DescriptionAttribute("application/pdf")]
            PDF,
            [DescriptionAttribute("text/plain")]
            TXT,
            [DescriptionAttribute("application/doc")]
            DOC,
            [DescriptionAttribute("application/docx")]
            DOCX,
            [DescriptionAttribute("application/vnd.ms-excel")]
            XLS,          
            [DescriptionAttribute("image/jpg")]
            JPG,
            [DescriptionAttribute("image/png")]
            PNG
        }
        #region Helper Functions

        public static string Description(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            DescriptionAttribute attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }
        #endregion
    }
}