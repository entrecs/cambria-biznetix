﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biznetix.BizCMS.Core.DataTables
{
    public class Search
    {
        /// <summary>
        /// Gets the value of the search.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Indicates if the value of the search is a regex value or not.
        /// </summary>
        public bool Regex { get; set; }
    }
}