﻿using System;
using System.Collections.Generic;

namespace Biznetix.BizCMS.Core.DataTables
{
    public class DataTablesRequest
    {
        /// <summary>
        /// Gets/Sets the draw counter from DataTables.
        /// </summary>
        public int Draw { get; set; }

        /// <summary>
        /// Gets/Sets the start record number (jump) for paging.
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Gets/Sets the length of the page (paging).
        /// </summary>
        public int Length { get; set; }

        public IList<Order> Order { get; set; }

        /// <summary>
        /// Gets/Sets the global search term.
        /// </summary>
        public Search Search { get; set; }

        /// <summary>
        /// Gets/Sets the column collection.
        /// </summary>
        public IList<Column> Columns { get; set; }
    }
}
