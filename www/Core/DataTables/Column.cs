﻿using System;
using System.Collections.Generic;

namespace Biznetix.BizCMS.Core.DataTables
{
    public class Column
    {
        /// <summary>
        /// Gets the data component (bind property name).
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Gets the name component (if any provided on client-side script).
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Indicates if the column is searchable or not.
        /// </summary>
        public bool Searchable { get; set; }

        /// <summary>
        /// Indicates if the column is orderable or not.
        /// </summary>
        public bool Orderable { get; set; }

        /// <summary>
        /// Gets the search component for the column.
        /// </summary>
        public Search Search { get; set; }
    }
}
