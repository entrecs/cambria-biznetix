﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Biznetix.BizCMS.Data;

namespace Biznetix.BizCMS.Core
{
    public class DataUtils
    {
        public static PetaPoco.Page<T> PagedQuery<T>(Core.DataTables.DataTablesRequest dtRequest) where T : DefaultConnectionDB.Record<T>, new()
        {
            PetaPoco.Sql pageQuery = new PetaPoco.Sql();
            pageQuery.Append("WHERE 1=1 ");

            System.Reflection.PropertyInfo[] itemProperties = typeof(T).GetProperties();
            if (dtRequest!=null && !String.IsNullOrEmpty(dtRequest.Search.Value))
            {
                pageQuery.Append("AND (1=0 ");


                foreach (Core.DataTables.Column c in dtRequest.Columns)
                {
                    System.Reflection.PropertyInfo propertyInfo = itemProperties.SingleOrDefault(p => p.Name == c.Data);

                    if (propertyInfo == null)
                        continue;
                    if (c.Searchable)
                    {
                        if (!String.IsNullOrEmpty(c.Data) && !String.IsNullOrEmpty(dtRequest.Search.Value))
                        {
                            if (propertyInfo.PropertyType == typeof(String))
                                pageQuery.Append(String.Format("OR {0} LIKE '%{1}%'", c.Name, dtRequest.Search.Value));
                            else if (propertyInfo.PropertyType.UnderlyingSystemType == typeof(DateTime))
                                pageQuery.Append(String.Format("OR {0} = '{1}'", c.Name, dtRequest.Search.Value));

                            else
                            {
                                if (Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null)
                                {
                                    try
                                    {
                                        var val = Convert.ChangeType(dtRequest.Search.Value, Nullable.GetUnderlyingType(propertyInfo.PropertyType));
                                        pageQuery.Append(String.Format("OR {0} = {1}", c.Name, val));
                                    }
                                    catch { }
                                }
                                else
                                {
                                    try
                                    {
                                        var val = Convert.ChangeType(dtRequest.Search.Value, propertyInfo.PropertyType);
                                        pageQuery.Append(String.Format("OR {0} = {1}", c.Name, val));
                                    }
                                    catch { }
                                }
                            }
                        }

                    }
                }
                pageQuery.Append(") ");
            }
            pageQuery.Append("AND (1=1 ");
            foreach (Core.DataTables.Column c in dtRequest.Columns)
            {
                System.Reflection.PropertyInfo propertyInfo = itemProperties.SingleOrDefault(p => p.Name == c.Data);

                if (propertyInfo == null)
                    continue;

                if (!String.IsNullOrEmpty(c.Data) && !String.IsNullOrEmpty(c.Search.Value ))
                {
                    if (propertyInfo.PropertyType == typeof(String))
                        pageQuery.Append(String.Format("AND {0} LIKE '%{1}%'", c.Name, c.Search.Value));
                    else
                    {
                        if (Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null)
                        {
                            try
                            {
                                var val = Convert.ChangeType(c.Search.Value, Nullable.GetUnderlyingType(propertyInfo.PropertyType));
                                pageQuery.Append(String.Format("AND {0} = {1}", c.Name, val));
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                var val = Convert.ChangeType(c.Search.Value, propertyInfo.PropertyType);
                                pageQuery.Append(String.Format("AND {0} = {1}", c.Name, val));
                            }
                            catch { }
                        }

                    }
                }
            }
            pageQuery.Append(") ");


            foreach (Core.DataTables.Order orderData in dtRequest.Order)
            {
                string colName = dtRequest.Columns[orderData.Column].Data;

                System.Reflection.PropertyInfo propertyInfo = itemProperties.SingleOrDefault(p => p.Name == colName);

                if (propertyInfo == null)
                    continue;

                pageQuery.OrderBy(String.Format("{0} {1}", colName, orderData.Dir));
            }

            PetaPoco.Page<T> queryResults = DefaultConnectionDB.Record<T>.Page((dtRequest.Start / dtRequest.Length) + 1, dtRequest.Length, pageQuery);

            return queryResults;
        }
    }
}